using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityMeshSimplifier;

public class CreateAvater : MonoBehaviour
{
    public static int avatercount = 100;

    public CharacterContainer contain;

    public int count = 0;

    public MB3_MeshBaker optimizer;

    public bool isoptimzing;

    public InputField input;

    public Material mat;

    public enum LoadType
    {
        Normal,
        MeshOpti,
        ShadowOpti,
        LODOpti
    }

    public static LoadType type = LoadType.Normal;


    IEnumerator Start()
    {
        yield return null;

        for (int handidcount = 111000100; handidcount <= 111000300; handidcount += 100)
        {
            for (int shoesid = 104000100; shoesid <= 104000800; shoesid += 100)
            {
                for (int bottomidcount = 103000100; bottomidcount <= 103000700; bottomidcount += 100)
                {
                    for (int topidcount = 102000100; topidcount <= 102000700; topidcount += 100)
                    {
                        for (int hairidcount = 101000100; hairidcount <= 101000800; hairidcount += 100)
                        {
                            if (count >= avatercount)
                                break;

                            float x = count % 10;
                            float z = count / 10;

                            contain.spawnPos = new Vector3(138f + x, 0, 95f + z);

                            contain.equipItemsList[Const.BagTabOrder.Hair] = hairidcount;
                            contain.equipItemsList[Const.BagTabOrder.Top] = topidcount;
                            contain.equipItemsList[Const.BagTabOrder.Bottom] = bottomidcount;
                            contain.equipItemsList[Const.BagTabOrder.Shoes] = shoesid;
                            contain.equipItemsList[Const.BagTabOrder.Hand] = handidcount;

                            contain.CreateDummyCharacter();

                            count++;


                            //yield return null;
                        }
                    }
                }
            }
        }


        switch (type)
        {
            case LoadType.MeshOpti:
            case LoadType.ShadowOpti:
                yield return OptimizaingRoutine();
                break;
            case LoadType.LODOpti:
                foreach (var data in contain.dummyCharacters)
                {
                    var lodGeneratorHelper = data.gameObject.AddComponent<UnityMeshSimplifier.LODGeneratorHelper>();

                    //lodGeneratorHelper.ResetRuntime();

                    LODLevel[] levels = new LODLevel[]
                    {
                        new LODLevel(0.5f, 1f)
                        {
                            CombineMeshes = false,
                            CombineSubMeshes = false,
                            SkinQuality = SkinQuality.Auto,
                            ShadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On,
                            ReceiveShadows = true,
                            SkinnedMotionVectors = true,
                            LightProbeUsage = UnityEngine.Rendering.LightProbeUsage.BlendProbes,
                            ReflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.BlendProbes,
                        },
                        new LODLevel(0.17f, 0.65f)
                        {
                            CombineMeshes = true,
                            CombineSubMeshes = false,
                            SkinQuality = SkinQuality.Auto,
                            ShadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On,
                            ReceiveShadows = true,
                            SkinnedMotionVectors = true,
                            LightProbeUsage = UnityEngine.Rendering.LightProbeUsage.BlendProbes,
                            ReflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Simple
                        },
                        //new LODLevel(0.02f, 0.4225f)
                        new LODLevel(0.05f, 0.4225f)
                        {
                            CombineMeshes = true,
                            CombineSubMeshes = true,
                            SkinQuality = SkinQuality.Bone2,
                            ShadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On,
                            ReceiveShadows = false,
                            SkinnedMotionVectors = false,
                            LightProbeUsage = UnityEngine.Rendering.LightProbeUsage.Off,
                            ReflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Off
                        },
                        new LODLevel(0.02f, 0.2225f)
                        {
                            CombineMeshes = true,
                            CombineSubMeshes = true,
                            SkinQuality = SkinQuality.Bone2,
                            ShadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off,
                            ReceiveShadows = false,
                            SkinnedMotionVectors = false,
                            LightProbeUsage = UnityEngine.Rendering.LightProbeUsage.Off,
                            ReflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Off
                        }
                    };

                    lodGeneratorHelper.Levels = levels;


                    UnityMeshSimplifier.LODGenerator.GenerateLODs(lodGeneratorHelper);
                }
                break;
        }
    }

    public void OnButtonNormal()
    {
        avatercount = int.Parse(input.text);


        type = LoadType.Normal;

        SceneLoader.Instance.LoadScene("HongDae");
    }
    public void OnButtonOpti()
    {

        avatercount = int.Parse(input.text);

        type = LoadType.MeshOpti;

        SceneLoader.Instance.LoadScene("HongDae");
    }
    public void OnButtonShadow()
    {

        avatercount = int.Parse(input.text);

        type = LoadType.ShadowOpti;

        SceneLoader.Instance.LoadScene("HongDae");
    }

    public void OnButtonLOD()
    {
        avatercount = int.Parse(input.text);

        type = LoadType.LODOpti;

        SceneLoader.Instance.LoadScene("HongDae");
    }

    public void OnButtonLightOn()
    {
        //switch (type)
        //{
        //    case LoadType.Normal:
        //        foreach (var data in contain.dummyCharacters)
        //        {
        //            data.GetComponent<Renderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
        //        }
        //            break;
        //    case LoadType.MeshOpti:
        //        foreach (var data in opties)
        //        {
        //            data.GetComponent<Renderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
        //        }
        //        break;
        //    case LoadType.ShadowOpti:
        //        foreach (var data in shadows)
        //        {
        //            data.gameObject.SetActive(true);
        //        }
        //        break;
        //}
    }
    public void OnButtonLightOFF()
    {
        //switch (type)
        //{
        //    case LoadType.Normal:
        //        foreach (var data in contain.dummyCharacters)
        //        {
        //            data.GetComponent<Renderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        //        }
        //        break;
        //    case LoadType.MeshOpti:
        //        foreach (var data in opties)
        //        {
        //            data.GetComponent<Renderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        //        }
        //        break;
        //    case LoadType.ShadowOpti:
        //        foreach (var data in shadows)
        //        {
        //            data.gameObject.SetActive(false);
        //        }
        //        break;
        //}
    }

    //public void OnValidate()
    //{
    //    if(Application.isPlaying)
    //    {
    //        if (isoptimzing)
    //        {
    //            isoptimzing = false;

    //            //contain.LoadCharacter(95, "aaaaa", 157, 2, null, new int[] { 101000300, 102000300, 103000300, 104000200, 111000300 }, 138, 0, 95);

    //            //contain.CreateDummyCharacter();

    //            Optimizing();

    //        }
    //    }
    //}



    public void Optimizing()
    {
        StopAllCoroutines();

        StartCoroutine(OptimizaingRoutine());
    }

    IEnumerator OptimizaingRoutine()
    {
        yield return null;

        foreach (var data in contain.dummyCharacters)
        {
            var cloptimizer = GameObject.Instantiate(optimizer, data.transform);

            var rends = data.GetComponentsInChildren<SkinnedMeshRenderer>();

            cloptimizer.ClearMesh();

            cloptimizer.objsToMesh.Clear();

            GameObject[] objs = new GameObject[rends.Length];

            for (int i = 0; i < rends.Length; i++)
            {
                objs[i] = rends[i].gameObject;
                cloptimizer.objsToMesh.Add(objs[i]);
            }

            cloptimizer.parentSceneObject = data.transform;
            cloptimizer.meshCombiner.resultSceneObject = cloptimizer.gameObject;

            //string opname = optimizer.name;
            //optimizer.name = data.name + " : " + count + " : ";

            cloptimizer.AddDeleteGameObjects(objs, null, true);
            //optimizer.name = opname;

            cloptimizer.Apply();

            data.GetComponent<AvatarSetting>().animator.cullingMode = AnimatorCullingMode.AlwaysAnimate;


            opties.Add(cloptimizer.meshCombiner.targetRenderer);

            //-------------------------------------------------------------------------------------------------
            if (type == LoadType.ShadowOpti)
            {
                cloptimizer.meshCombiner.targetRenderer.GetComponent<Renderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

                var sdwcloptimizer = GameObject.Instantiate(optimizer, data.transform);

                var sdwrends = data.GetComponentsInChildren<SkinnedMeshRenderer>();

                sdwcloptimizer.ClearMesh();

                sdwcloptimizer.objsToMesh.Clear();

                GameObject[] sdwobjs = new GameObject[sdwrends.Length];

                for (int i = 0; i < sdwrends.Length; i++)
                {
                    sdwobjs[i] = sdwrends[i].gameObject;
                    sdwcloptimizer.objsToMesh.Add(sdwobjs[i]);
                }

                sdwcloptimizer.parentSceneObject = data.transform;
                sdwcloptimizer.meshCombiner.resultSceneObject = sdwcloptimizer.gameObject;

                //string opname = optimizer.name;
                //optimizer.name = data.name + " : " + count + " : ";

                sdwcloptimizer.AddDeleteGameObjects(objs, null, true);
                //optimizer.name = opname;

                sdwcloptimizer.Apply();



                //data.GetComponent<AvatarSetting>().animator.cullingMode = AnimatorCullingMode.AlwaysAnimate;

                //var shadowobj = GameObject.Instantiate(cloptimizer.meshCombiner.targetRenderer.gameObject, cloptimizer.meshCombiner.targetRenderer.transform);

                shadows.Add(sdwcloptimizer.meshCombiner.targetRenderer);

                var localmats = sdwcloptimizer.meshCombiner.targetRenderer.GetComponent<SkinnedMeshRenderer>().materials;

                for (int i = 0; i < localmats.Length; i++)
                {
                    localmats[i] = mat;
                }

                sdwcloptimizer.meshCombiner.targetRenderer.GetComponent<SkinnedMeshRenderer>().materials = localmats;


                sdwcloptimizer.meshCombiner.targetRenderer.GetComponent<Renderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            }

        }

        Debug.Log(" Done !!! ");
        yield return null;
    }
    
    List<Renderer> origin = new List<Renderer>();
    List<Renderer> opties = new List<Renderer>();
    List<Renderer> shadows = new List<Renderer>();


}
