##  Infra 작업 md파일 by leeyounghoon

## VPC

> `SuperKola-VPC-Live-vpc`
```log

* VPC 
SuperKola-VPC-Live-vpc
    + 세부 정보
        VPC ID:            vpc-0372f24bfb9512008 
        DHCP 옵션 세트:     dopt-0dd20aca58ced33a9
        기본 라우팅 테이블:  rtb-05cad026f23ec4893
        기본 네트워크 ACL:   acl-0cf4812fa7a907fb1 / Superkola-VPC-Network-ACL


        * DHCP 옵션
            dopt-0dd20aca58ced33a9
                domain-name: ap-southeast-1.compute.internal
                domain-name-servers: AmazonProvidedDNS

                + 세부정보
                    도메인 이름: ap-southeast-1.compute.internal
                    도메인 이름 서버 : AmazonProvidedDNS

        * 라우팅 테이블
            rtb-05cad026f23ec4893
                + 세부정보
                    VPC: vpc-0372f24bfb9512008 | SuperKola-VPC-Live-vpc
                + 라우팅
                    172.32.0.0/16   local

        * 네트워크 ACL
            acl-0cf4812fa7a907fb1 / Superkola-VPC-Network-ACL
                + 인바운드규칙
                    100 모든 트래픽
                + 아웃바운드 규칙
                    100 모든 트래픽
                + 서브넷 연결
```

![Live VPC 구성](_images/2024-10-15-18-27-59.png)


> `구성 요약`
```log

* VPC

SuperKola-VPC-Live-vpc

* Subnet

- ap-southeast-1a 
    SuperKola-VPC-Live-subnet-public1-ap-southeast-1a
    SuperKola-VPC-Live-subnet-private2-ap-southeast-1b  

- ap-southeast-1b
    SuperKola-VPC-Live-subnet-public2-ap-southeast-1b   
    SuperKola-VPC-Live-subnet-private1-ap-southeast-1a


* Routing Table

SuperKola-VPC-Live-rtb-public
SuperKola-VPC-Live-rtb-private1-ap-southeast-1a
SuperKola-VPC-Live-rtb-private2-ap-southeast-1b


[ 네트워크 연결 ]

* 인터넷 게이트 웨이
    SuperKola-VPC-Live-igw      - 2개의 퍼블릭 서브넷으로 인터넷 라우팅 , 인터넷으로 0개의 프라이빗 서브넷 라우팅

* 엔드포인트
    SuperKola-VPC-Live-vpce-s3  - S3에 대한 게이트웨이 엔드포인트
        - 서비스 이름: com.amazonaws.ap-southeast-1.s3
        - 엔드 포인트 유형 : GateWay

```

> `네트워크 연결 구성`

- SuperKola-VPC-Live-igw( public ip 접근 을 위한 게이트웨이)
![public ip 접근 을 위한 게이트웨이](_images/2024-10-16-10-15-38.png)

- s3 게이트웨이 엔드포인트 ( SuperKola-VPC-Live-vpce-s3 )
![s3용 게이트웨이 엔드포인트](_images/2024-10-16-10-13-46.png)

```log

인터넷 게이트웨이(IGW):

역할: IGW는 VPC 내의 리소스가 인터넷과 통신할 수 있도록 해주는 게이트웨이입니다. 
이를 통해 퍼블릭 서브넷에 위치한 인스턴스들은 인터넷에 접속할 수 있고, 외부에서 이 인스턴스들에 접근할 수 있습니다.

구성 이유: 퍼블릭 서브넷에 배치된 서버가 외부 네트워크와 통신하거나, 외부 클라이언트가 해당 서버에 접근해야 하는 경우, IGW를 통해 인터넷과 연결됩니다. 
예를 들어, 웹 서버 같은 경우 인터넷에서 접근할 수 있어야 하므로 IGW가 필요합니다.

VPC 엔드포인트(S3):

역할: VPC 엔드포인트는 VPC 내부에서 AWS 서비스(S3와 같은)와 연결할 수 있도록 해줍니다.
엔드포인트를 사용하면 VPC 내의 리소스가 인터넷을 경유하지 않고도 AWS 서비스에 안전하게 접속할 수 있습니다.

구성 이유: 프라이빗 서브넷에 있는 리소스가 S3 버킷과 통신할 때, 인터넷을 거치지 않고 안전하게 AWS의 내부 네트워크를 통해 접속할 수 있도록 하기 위해 사용됩니다. 
이 경우, 보안과 비용을 절약할 수 있습니다.

결론:

IGW는 인터넷과의 연결을 위한 것이며, 주로 퍼블릭 서브넷에서 사용됩니다.
**VPC 엔드포인트(S3)**는 인터넷을 거치지 않고도 S3와 같은 AWS 서비스를 접근하기 위한 목적으로 사용되며, 주로 프라이빗 서브넷에서 사용됩니다.

```


> `이해정리 - VPC -> EC2 , RDS `

```log
| VPC 역할 및 관계 - ( SuperKola-VPC-Live-vpc ) 사용하는곳은 Live EC2와 RDS 에서 사용되고 있다. 
| 네트워크 망을 구성을 한다.


- 네트워크 관리 및 제어
인터넷 및 AWS 서비스 접근 제어: 인터넷 게이트웨이(IGW)를 통해 퍼블릭 서브넷의 인스턴스들이 인터넷에 접근하도록 설정하거나, 
VPC 엔드포인트를 통해 인터넷을 거치지 않고 AWS 서비스(S3, DynamoDB 등)에 안전하게 접근할 수 있습니다.

- 비용 절감
데이터 전송 비용 절감: VPC 엔드포인트를 사용하면 인터넷을 경유하지 않고 
AWS 서비스와 직접 연결되기 때문에, 데이터 전송 비용을 줄일 수 있습니다.

- VPC 구성의 대표적 시나리오:
웹 애플리케이션을 배포할 때 웹 서버는 퍼블릭 서브넷에, 데이터베이스 서버는 프라이빗 서브넷에 배치하여 외부 공격으로부터 보호하면서, 
안전하게 데이터베이스에 접근할 수 있게 함.

온프레미스 시스템과 AWS 클라우드 리소스를 안전하게 연동하여 하이브리드 클라우드 환경을 구성.

```

> `이해정리 - VPC, 서브넷, 라우팅 테이블, 네트워크 연결 `

```log


1. VPC (Virtual Private Cloud)
SuperKola-VPC-Live:
VPC는 AWS에서 네트워크를 격리하는 공간입니다. 이 안에서 서버, 데이터베이스 등을 배치하고 관리할 수 있어요.
이 VPC 안에는 **두 개의 가용 영역(ap-southeast-1a, ap-southeast-1b)**에 각각 서브넷이 배치되어 있어요. 즉, 동일한 리소스를 물리적으로 분산시켜, 가용성과 안정성을 높이는 구조입니다.

2. 서브넷

Public Subnet (공개 서브넷):
    SuperKola-VPC-Live-subnet-public-1a (ap-southeast-1a)
    SuperKola-VPC-Live-subnet-public-1b (ap-southeast-1b)
역할:
공개 서브넷은 인터넷과 직접 연결되는 네트워크입니다. 웹 서버나 로드 밸런서처럼 외부에서 접근해야 하는 리소스를 배치합니다.

Private Subnet (비공개 서브넷):
    SuperKola-VPC-Live-subnet-private1-ap-southeast-1a
    SuperKola-VPC-Live-subnet-private2-ap-southeast-1b
역할:
비공개 서브넷은 외부에서 직접 접근할 수 없는 내부 네트워크입니다. 데이터베이스나 애플리케이션 서버와 같이 인터넷과의 직접적인 연결이 필요 없는 리소스를 배치합니다. 외부로 나가야 할 경우 NAT 게이트웨이를 통해 트래픽이 나가죠.


- 물리적 공간의 위치 (ap-southeast-1a , ap-southeast-1b)
// 용어 : 가용 영역 : AZ (Availibity zone) 복수개도 가능 : Availibity Zone A , Availibity Zone B , Availibity Zone C

ap-southeast-1a 
    SuperKola-VPC-Live-subnet-public1-ap-southeast-1a
    SuperKola-VPC-Live-subnet-private2-ap-southeast-1b  

ap-southeast-1b
    SuperKola-VPC-Live-subnet-public2-ap-southeast-1b   
    SuperKola-VPC-Live-subnet-private1-ap-southeast-1a

// 같은 싱가폴에 IDC센터를 두곳 (ap-southeast-1a ,ap-southeast-1b )으로  지정하여 한곳에 문제가 생겨도 다른곳에서 대응할수 있도록 구성함.

3. 라우팅 테이블
Public Route Table:
    SuperKola-VPC-Live-rtb-public
이 라우팅 테이블은 공개 서브넷의 트래픽을 처리합니다. 모든 트래픽이 **인터넷 게이트웨이(IGW)**를 통해 인터넷으로 나가도록 설정되어 있어요.

Private Route Table:
    SuperKola-VPC-Live-rtb-private1-ap-southeast-1a
    SuperKola-VPC-Live-rtb-private2-ap-southeast-1b
이 라우팅 테이블은 비공개 서브넷의 트래픽을 처리합니다. 외부로 나가야 하는 트래픽은 NAT 게이트웨이를 통해 나가고, 인터넷과 직접 연결되지 않습니다.

4. 네트워크 연결
인터넷 게이트웨이 (IGW):
    SuperKola-VPC-Live-igw
역할:
VPC 안의 공개 서브넷에 있는 리소스들이 인터넷에 접근할 수 있게 해줍니다. 예를 들어, 웹 서버가 외부 사용자의 요청을 받기 위해 인터넷과 연결되려면 이 인터넷 게이트웨이가 필요해요.

S3 엔드포인트:
    SuperKola-VPC-Live-vpce-s3
역할:
이 엔드포인트는 VPC 내부에서 S3 버킷에 접근할 때 사용됩니다. 엔드포인트를 통해, VPC의 리소스가 인터넷을 거치지 않고도 S3와 통신할 수 있어요. 즉, 데이터 전송이 더 빠르고 안전해지죠.


정리:
VPC: SuperKola-VPC-Live라는 네트워크 공간 안에 여러 리소스들이 배치되어 있음.
공개 서브넷: 외부 인터넷과 연결되어 있고, 웹 서버 같은 리소스들이 배치됨.
비공개 서브넷: 외부에서 접근 불가, 내부 애플리케이션이나 데이터베이스가 배치됨. NAT 게이트웨이로 외부로 나갈 수 있음.
인터넷 게이트웨이(IGW): 공개 서브넷의 리소스가 외부 인터넷과 통신할 수 있도록 해줌.
S3 엔드포인트: 비공개 서브넷에서 인터넷을 거치지 않고 S3 버킷에 접근할 수 있게 해줌.

```

> `참고 - 구성 상세 정보`
```log

--- 서브넷 구성 상세

Name, 서브넷 ID, VPC, IPv4, CIDR, 가용 영역, 가용 영역 ID, 네트워크 경계 그룹, 라우팅 테이블, 네트워크 ACL

SuperKola-VPC-Live-subnet-public1-ap-southeast-1a   
    subnet-01a9f058a8b697e77
    vpc-0372f24bfb9512008 | SuperKola-VPC-Live-vpc  
    172.32.0.0/20
    ap-southeast-1a 
    apse1-az1   
    ap-southeast-1  
    rtb-04e41e86ea570396f | SuperKola-VPC-Live-rtb-public   
    acl-0cf4812fa7a907fb1 | Superkola-VPC-Network-ACL

SuperKola-VPC-Live-subnet-private2-ap-southeast-1b  
    subnet-0cc4b4d0ac597f912    
    vpc-0372f24bfb9512008 | SuperKola-VPC-Live-vpc  
    172.32.144.0/20     
    ap-southeast-1b 
    apse1-az2   
    ap-southeast-1  
    rtb-05fc6980e4ea98709 | SuperKola-VPC-Live-rtb-private2-ap-southeast-1b 
    acl-0cf4812fa7a907fb1 | Superkola-VPC-Network-ACL

SuperKola-VPC-Live-subnet-public2-ap-southeast-1b   
    subnet-07467998ae89d6a32    
    vpc-0372f24bfb9512008 | SuperKola-VPC-Live-vpc  
    172.32.16.0/20  
    ap-southeast-1b 
    apse1-az2   
    ap-southeast-1  
    rtb-04e41e86ea570396f | SuperKola-VPC-Live-rtb-public   
    acl-0cf4812fa7a907fb1 | Superkola-VPC-Network-ACL

SuperKola-VPC-Live-subnet-private1-ap-southeast-1a  
    subnet-02a13ca112d48949e    
    vpc-0372f24bfb9512008 | SuperKola-VPC-Live-vpc  
    172.32.128.0/20 
    ap-southeast-1a 
    apse1-az1   
    ap-southeast-1  
    rtb-0b6d6e43e6e50a9ab | SuperKola-VPC-Live-rtb-private1-ap-southeast-1a 
    acl-0cf4812fa7a907fb1 | Superkola-VPC-Network-ACL


--- 라우팅 테이블 구성 상세

Name                                            라우팅 테이블 ID        명시적 서브넷 연결                                                                VPC                       소유자 ID
SuperKola-VPC-Live-rtb-public                   rtb-04e41e86ea570396f   2 서브넷                                                                        vpc-0372f24bfb9512008 | SuperKola-VPC-Live-vpc  
SuperKola-VPC-Live-rtb-private1-ap-southeast-1a rtb-0b6d6e43e6e50a9ab   subnet-02a13ca112d48949e / SuperKola-VPC-Live-subnet-private1-ap-southeast-1a   vpc-0372f24bfb9512008 | SuperKola-VPC-Live-vpc   
SuperKola-VPC-Live-rtb-private2-ap-southeast-1b rtb-05fc6980e4ea98709   subnet-0cc4b4d0ac597f912 / SuperKola-VPC-Live-subnet-private2-ap-southeast-1b   vpc-0372f24bfb9512008 | SuperKola-VPC-Live-vpc  

```

--------------------------------------------------------------------------------------

## EC2 

> `로드밸런서 및 대상그룹 ( Live WebServer 구성 및 요약 )`
```log
| web3 server 의 트래픽이 많이 질경우를 대비하여 트래픽 분산 목적으로 구성. 

SuperKola-Live-LB-Web3 ( 로드밸런서 )
    + 세부정보
        - VPC:  SuperKola-VPC-Live-vpc
    
    + 리스너규칙
        - 대상그룹: SuperKola-Live-SG-Web3
        - 리스너:   Https:443
        - 기본 SSL/TLS 인증서:  superkolatactics.io(인증서 ID: 622f1de5-33a0-4355-8079-810fc36d37d7)  // AWS Certificate Manager(ACM)


SuperKola-Live-SG-Web3 ( 대상 그룹 )
    + 대상
        - 등록된 대상:  superkola-game-live-ec2-web3-1 , Port:3000 , 영역:ap-southeast-1a
        - 등록된 대상:  superkola-game-live-ec2-web3-2 , Port:3000 , 영역:ap-southeast-1a ( 현재 삭제됨)

        //점검시 변경:  superkola-game-live-ec2-web3_1-linux



작동 방식 설명:

1. 클라이언트 요청: 클라이언트는 https://superkolatactics.io 도메인을 통해 HTTPS 요청을 보냅니다.

2. 로드 밸런서 처리: 로드 밸런서(SuperKola-Live-LB-Web3)는 HTTPS(포트 443) 리스너를 통해 해당 요청을 수신합니다.

3. SSL/TLS 암호화: 로드 밸런서는 클라이언트와 서버 간의 통신을 보호하기 위해 ACM에 등록된 superkolatactics.io 인증서를 사용하여 SSL/TLS 암호화 연결을 설정합니다.

4. 트래픽 분산: 로드 밸런서는 대상 그룹(SuperKola-Live-SG-Web3)에 등록된 superkola-game-live-ec2-web3-1 인스턴스로 트래픽을 분배합니다.
이때, superkola-game-live-ec2-web3-2 인스턴스는 대상 그룹에서 삭제되었기 때문에 해당 인스턴스에는 트래픽이 전달되지 않습니다.

5. 응답 반환: superkola-game-live-ec2-web3-1 서버가 요청을 처리한 후, 로드 밸런서를 통해 클라이언트에게 응답을 보냅니다.
```

> `이해정리 -  EC2 로드밸런서 , Route 53`
```log

관계:
1. EC2 로드밸런서:  분산할 EC2의 그룹화 한다.
2. Route 53: 외부로 부터 접근할 DNS를 설정 하고 , 로드밸런서에 연결한다.


- 로드 밸런서 종류 : AWS ELB(Elastic Load Balancer) 에는 다음과 같은 세 가지 유형이 있습니다

Application Load Balancer (ALB) : HTTP와 HTTPS 트래픽에 최적화되어 있으며
Network Load Balancer (NLB) : TCP, UDP, TLS 트래픽에 최적화되어 있으며
Classic Load Balancer (CLB) : 초기 ELB 서비스로, 단순한 로드 밸런싱 기능을 제공합니다


``` 

--------------------------------------------------------------------------------------

## Route 53  : superkolatactics.io  퍼블릭    

> `구성 요약`
```log

* 대시 보드 > DNS 관리 > superkolatactics.io(퍼블릭)

    레코드 이름                             레코드 유형  
    cms-recv-dev.superkolatactics.io        CNAME     ec2-15-165-125-176.ap-northeast-2.compute.amazonaws.com
    cms-recv-qa.superkolatactics.io         A         54.151.190.86
    cms-recv.superkolatactics.io            CNAME     SuperKola-Live-LB-Web3-895228234.ap-southeast-1.elb.amazonaws.com

    web3-recv-dev.superkolatactics.io       CNAME     ec2-43-201-89-210.ap-northeast-2.compute.amazonaws.com    
    web3-recv-qa.superkolatactics.io        CNAME     ec2-52-77-184-129.ap-southeast-1.compute.amazonaws.com    
    web3-recv.superkolatactics.io           CNAME     SuperKola-Live-LB-Web3-895228234.ap-southeast-1.elb.amazonaws.com


--- 도메인, DNS, Route53 - 정리 

도메인 (Domain)
- 계층적 구조: 도메인은 계층적으로 구성됩니다. 
  example.com 에서 com은 최상위 도메인(TLD), example은 그 하위의 도메인입니다.
  docs.aws.amazon.com: 여러 하위 도메인이 계층적으로 연결된 형태

DNS (Domain Name System)
- 도메인 이름을 IP 주소로 변환
- 계층적 구조: DNS는 계층적으로 구성되어 있습니다. 루트 DNS, 최상위 도메인(TLD), 하위 도메인 등의 구조를 통해 도메인 이름을 체계적으로 관리합니다.
- DNS 레코드: DNS에는 다양한 레코드 타입이 있습니다. 
  예를 들어, "A 레코드는 도메인 이름을 IPv4 주소로 매핑"하고, "MX 레코드는 이메일 서버를 정의"하는 등의 역할을 합니다.
- 트래픽 분산: DNS 라운드 로빈 방식을 사용하여 여러 서버에 트래픽을 분산시킬 수 있습니다.

(comment: 그래서 이거 가능 cms-recv-dev.superkolatactics.io, web3-recv-dev.superkolatactics.io )

Route 53
- DNS 관리: 도메인의 DNS 레코드를 관리합니다. 
  예를 들어, 도메인의 "A 레코드, CNAME 레코드, MX 레코드" 등을 Route 53에서 설정할 수 있습니다.
- 트래픽 라우팅: Route 53은 다양한 라우팅 정책을 제공합니다. 
  지연 시간 기반 라우팅, 가중치 라우팅, 장애 복구 라우팅 등을 통해 트래픽을 효율적으로 분산할 수 있습니다.

```

--------------------------------------------------------------------------------------

##  API GateWay

```log
API 리스트: 

Superkola-Live-Gateway-API      Created by AWS Lambda    REST    Edge    2024-02-14
Superkola-QA-Gateway-API        Created by AWS Lambda    REST    Regional    2024-04-11

구성:
Superkola-QA-Gateway 
    * 리소스
        + URL 호출 : https://ensa55vrc9.execute-api.ap-southeast-1.amazonaws.com/default
    * 스테이지
        + default
            + /
                + /Superkola-QA-Gateway
                    + Delete
                    + Get
                        + URL호출 : https://ensa55vrc9.execute-api.ap-southeast-1.amazonaws.com/default/Superkola-QA-Gateway
                    + Post                 
    * API 설정 
        + 기본 엔드포인트 : https://ensa55vrc9.execute-api.ap-southeast-1.amazonaws.com

// 접근 URL : https://ensa55vrc9.execute-api.ap-southeast-1.amazonaws.com/default/Superkola-QA-Gateway

```

##  Lambda

```log

함수 - 리스트: 

Superkola-Live-Gateway  Node.js 18.x
Superkola-QA-Gateway    Node.js 18.x

구성:

Superkola-QA-Gateway
    * 구성
        * 트리거
            + API 게이트웨이: Superkola-QA-Gateway-API
                arn:aws:execute-api:ap-southeast-1:336066227250:ensa55vrc9/*/*/Superkola-QA-Gateway
                API 엔드포인트: https://ensa55vrc9.execute-api.ap-southeast-1.amazonaws.com/default/Superkola-QA-Gateway

                - 세부 정보 
                    리소스 경로: /Superkola-QA-Gateway
                    메서드: ANY
                    문 ID: lambda-b7447617-4ba0-4b2b-b987-1b83f080a1d6, 20300be8-8104-566d-a1cc-cf740a77c795
                    서비스 보안 주체: apigateway.amazonaws.com
                    스테이지: default
                    승인: NONE
                    API 유형: REST
                    isComplexStatement: 아니
        * 권한
            + 실행 역할                
                + "Superkola-QA-Gateway-role-f73k5ku0"  // Identity and Access Management(IAM) 권한설정
                    + 리소스 요약 : 함수에 액세스 권한이 있는 리소스 및 작업을 보려면 서비스를 선택합니다.
                        Amazone CloudWatch Logs
                    + 리소스 기반 정책 설명
                        문 ID                                           보안 주체                       조건         작업
                        lambda-b7447617-4ba0-4b2b-b987-1b83f080a1d6     apigateway.amazonaws.com       ArnLike      lambda:InvokeFunction                        
                        20300be8-8104-566d-a1cc-cf740a77c795            apigateway.amazonaws.com       ArnLike      lambda:InvokeFunction


* "Superkola-QA-Gateway-role-f73k5ku0" - (IAM) 권한내용
    + 권한        
        AWSLambdaVPCAccessExecutionRole                                     AWS 관리형
        AWSLambdaBasicExecutionRole-5963ee07-fb16-4a55-b984-0a40ff1aa580    고객 관리형 
            + 권한
                +허용
                    CloudWatch Logs     제한적: 쓰기        Multiple    None
            + 정책 버전
                + 버전1
                    {
                        "Version": "2012-10-17",
                        "Statement": [
                            {
                                "Effect": "Allow",
                                "Action": "logs:CreateLogGroup",
                                "Resource": "arn:aws:logs:ap-southeast-1:336066227250:*"
                            },
                            {
                                "Effect": "Allow",
                                "Action": [
                                    "logs:CreateLogStream",
                                    "logs:PutLogEvents"
                                ],
                                "Resource": [
                                    "arn:aws:logs:ap-southeast-1:336066227250:log-group:/aws/lambda/Superkola-QA-Gateway:*"
                                ]
                            }
                        ]
                    }
```

![api gateway](_images/2024-10-15-17-25-17.png)



>`Superkola-QA-Gateway > 구성 >  Superkola-QA-Gateway/index.mjs`

```javascript
import mysql from "mysql2/promise.js";

export const handler = async (event, context) => {
    
    console.log('Loading function' + event.body);
    
    // MySQL 연결 설정
    const connection_game = await mysql.createConnection({
        host: '172.31.22.3',
        user: 'root',
        password: 'sK123!@#',
        database: 'qa_game'
    });


    const connection_world = await mysql.createConnection({
        host: '172.31.22.3',
        user: 'root',
        password: 'sK123!@#',
        database: 'qa_world'
    });
    

    
    try {
        
        let apkInfo;

        if(typeof event.apkVersion != "undefined")
        {
            const [rows, ] = await connection_game.execute('SELECT * FROM clientversion WHERE apkVersion="'+event.apkVersion+'";');
            
            if(rows[0] == null || rows[0] == undefined)
            {
                apkInfo = { };
            }
            else
            {
                apkInfo = rows[0];
            }
        }
        else
        {
            /*
            apkInfo = {'action' : event.action};
        
            switch (event.action)
            {
                case 'select':
                    {
                        break;
                    }
                case 'insert':
                    {
                        let query = 'INSERT INTO clientversion (apkVersion, lastBundleVersion, loadBalancerURL, cheatMode, ValidAPKVersionMin, ValidAPKVersionMax) VALUES ("'+event.data.apkVersion+'","'+event.data.lastBundleVersion+'","'+event.data.loadBalancerURL+'",'+event.data.cheatMode+',"'+event.data.ValidAPKVersionMin+'","'+event.data.ValidAPKVersionMax+'");';
                        apkInfo['query'] = query;
                        
                        await connection_game.execute( query );
                        
                        break;
                    }
                case 'update':
                    {
                        //let query = 'UPDATE clientversion SET apkVersion="'+event.data.apkVersion+'",lastBundleVersion="'+event.data.lastBundleVersion+'",stage="'+event.data.stage+'",loadBalancerURL="'+event.data.loadBalancerURL+'",cheatMode='+event.data.cheatMode+' WHERE apkVersion="'+event.data.apkVersion+'");';
                        let query = 'UPDATE clientversion SET lastBundleVersion="'+event.data.lastBundleVersion+'",loadBalancerURL="'+event.data.loadBalancerURL+'",cheatMode='+event.data.cheatMode+',ValidAPKVersionMin="'+event.data.ValidAPKVersionMin+'",ValidAPKVersionMax="'+event.data.ValidAPKVersionMax+'" WHERE apkVersion="'+event.data.apkVersion+'";';
                        
                        apkInfo['query'] = query;
                        
                        console.error('executing query:', query);
                        
                        await connection_game.execute( query );
                        
                        break;
                    }
                case 'delete':
                    {
                        let query = 'DELETE FROM clientversion WHERE apkVersion="'+event.data.apkVersion+'";';
                        apkInfo['query'] = query;
                        
                        await connection_game.execute(query);
                        
                        break;
                    }
            }
            
            // 쿼리 실행
            //const [rows, ] = await connection.execute('SELECT * FROM clientversion WHERE apkVersion = "'+event.apkVersion+'";');
            const [rows, ] = await connection_game.execute('SELECT * FROM clientversion ;');
            
            apkInfo['result'] = rows;            
            //apkInfo.push({"rows" : rows});
            
            if (rows.length > 0)
            {                
                for (let i in rows )
                {
                    let addobject = { };
                    
                    addobject[rows[i].key] = rows[i].value;                    
                }
            }
            */
        }        
        
        //const [rows2, ] = await connection.execute('SELECT * FROM serverinfoetc WHERE `key` in ("IsMaintenance", "ValidAPKVersionMin", "ValidAPKVersionMax");');
        const [rows2, ] = await connection_world.execute('SELECT * FROM serverinfoetc ;');
        
        const result = {
            apkInfo,
            global : { }
        }
        
        for (let x in rows2) {
            result.global[rows2[x].key] = rows2[x].value;
        }
        
        if (result.global.IsMaintenance.toLowerCase() === "true") {
            result.global.IsMaintenance = true;
        } else {
            result.global.IsMaintenance = false;
        }
        return result;
    } catch (error) {
        // 에러 처리
        //console.error('Error executing query:', error);
        
        const result = {
            apkInfo: {},
            global: {
                IsMaintenance: true,
                MaintenanceContents: " empty ",
                MaintenanceURL: " empty "
            },
            errorlog : error
        }
        return result;
        
    } finally {
        // 연결 종료
        await connection_game.end();
        await connection_world.end();
    }
};

```

--------------------------------------------------------------------------------------

## Amazon RDS - 데이터 베이스 ( Aurora MySql 8.0 )

> `구성 요약`
```log

superkola-rds-live
    + 관련
        DB 식별자                                            상태          역할                엔진                리전 및 AZ         크기
        superkola-rds-live                                  사용 가능       리전 클러스터       Aurora MySQL        ap-southeast-1      2 인스턴스
        superkola-rds-live-instance-1                       사용 가능       라이터 인스턴스     Aurora MySQL        ap-southeast-1a     서버리스 v2(ACU 0.5~128개)
        superkola-rds-live-instance-1-ap-southeast-1b       사용 가능       리더 인스턴스       Aurora MySQL        ap-southeast-1b     서버리스 v2(ACU 0.5~128개)

    + 엔드포인트
        superkola-rds-live.cluster-ckxxoi78exjz.ap-southeast-1.rds.amazonaws.com    라이터 3306
        superkola-rds-live.cluster-ro-ckxxoi78exjz.ap-southeast-1.rds.amazonaws.com 읽기 3306

    + 프록시 
        프록시 식별자                    상태        엔진 패밀리
        superkola-live-rds-proxy        사용 가능    MariaDB 및 MySQL

    + 인스턴스 구성
        superkola-rds-live-instance-1  ( 라이터 인스턴스 )
        superkola-rds-live-instance-1-ap-southeast-1b ( 리더 인스턴스 )
            가용 영역
                ap-southeast-1a
            VPC
                SuperKola-VPC-Live-vpc (vpc-0372f24bfb9512008)
            서브넷 그룹
                default-vpc-0372f24bfb9512008
            서브넷
                subnet-02a13ca112d48949e
                subnet-0cc4b4d0ac597f912
                subnet-07467998ae89d6a32
                subnet-01a9f058a8b697e77

```

> `서브넷 그룹`
```log

default-vpc-0372f24bfb9512008
    + 세부정보
        VPC ID : vpc-0372f24bfb9512008 
        ARN : arn:aws:rds:ap-southeast-1:336066227250:subgrp:default-vpc-0372f24bfb9512008
    + 서브넷
        ap-southeast-1a     subnet-01a9f058a8b697e77        172.32.0.0/20       
        ap-southeast-1b     subnet-0cc4b4d0ac597f912        172.32.144.0/20
        ap-southeast-1b     subnet-07467998ae89d6a32        172.32.16.0/20
        ap-southeast-1a     subnet-02a13ca112d48949e        172.32.128.0/20

```

> `프록시`
```log

superkola-live-rds-proxy
    + 프록시 엔드포인트 ( 생성 )
        superkola-live-rds-proxy.proxy-ckxxoi78exjz.ap-southeast-1.rds.amazonaws.com    vpc-0372f24bfb9512008   사용 가능   읽기/쓰기
        superkola-live-rds-proxy-read-only.endpoint.proxy-ckxxoi78exjz.ap-southeast-1.rds.amazonaws.com vpc-0372f24bfb9512008   사용 가능   읽기 전용

    + 대상 그룹
        대상 그룹 식별자     연결된 데이터베이스 
        default             superkola-rds-live  

    + 인증
        보안 암호 ARN                                                                                클라이언트 인증 유형            IAM 인증        
        arn:aws:secretsmanager:ap-southeast-1:336066227250:secret:SuperKola-Live-RDS-Key-R12je8     MySQL Native password         허용되지 않음


```



--------------------------------------------------------------------------------------

## Amazon ElastiCache - Redis OSS 캐시

```log

superkola-game-qa-redis-7       Available       superkola-game-qa-redis-7       7.0.7       cache.t4g.micro     July 24, 2024, 15:02:43 (UTC+09:00)
    + 세부정보
        리더 엔드포인트  superkola-game-qa-redis-7-ro.vuis0q.ng.0001.apse1.cache.amazonaws.com:6379
    + 노드 
        primary superkola-game-qa-redis-7-002.vuis0q.0001.apse1.cache.amazonaws.com:6379
        replica superkola-game-qa-redis-7-001.vuis0q.0001.apse1.cache.amazonaws.com:6379


superkola-game-live-redis-7     Available       superkola-game-live-redis-7     7.0.7       cache.t4g.medium    July 24, 2024, 16:18:37 (UTC+09:00)
    + 세부정보
        리더 엔드포인트  superkola-game-live-redis-7-ro.vuis0q.ng.0001.apse1.cache.amazonaws.com:6379
    + 노드 
        primary superkola-game-live-redis-7-002.vuis0q.0001.apse1.cache.amazonaws.com:6379
        replica superkola-game-live-redis-7-001.vuis0q.0001.apse1.cache.amazonaws.com:6379

```


--------------------------------------------------------------------------------------

##  Amazon SQS 대기열

```log

DEV_LogQueue    https://sqs.ap-southeast-1.amazonaws.com/336066227250/DEV_LogQueue
QA_LogQueue     https://sqs.ap-southeast-1.amazonaws.com/336066227250/QA_LogQueue
LogQueue        https://sqs.ap-southeast-1.amazonaws.com/336066227250/LogQueue

```

--------------------------------------------------------------------------------------
## ETC ---  


### EC2 

> `인스턴스 - 시작 템플릿 `
> `이미지   - AMI`

|특징	              |  AMI	                                                          |   EC2 시작 템플릿 |
|---------------------|-----------------------------------------------------------------|--------------------|
|역할	              |  EC2 인스턴스의 운영 체제 및 소프트웨어 스택을 정의함	          |  EC2 인스턴스를 생성할 때 필요한 종합적인 설정 정보를 정의 |
|구성 요소            |  운영 체제, 소프트웨어, 인스턴스 스토리지 설정	                  |  AMI, 인스턴스 유형, VPC/서브넷, 보안 그룹, 키 페어 등 포함 |
|버전 관리	          |  AMI 자체에 대한 버전 관리 없음	                                  |  버전 관리가 가능하여 다양한 설정에 따라 템플릿을 관리할 수 있음 |
|사용 예	          |  동일한 소프트웨어 환경을 가진 인스턴스를 여러 개 생성할 때 사용	  |  동일한 AMI를 사용하면서도 다른 인스턴스 유형, 네트워크 설정 등을 쉽게 관리할 때 사용 |
|자동 확장과의 연계	  |  직접 사용하지 않음	                                              |  Auto Scaling과 Spot 인스턴스 요청에 쉽게 사용 가능 |
|변경 가능성	      |  생성 후 AMI 자체는 변경 불가 (새로 만들어야 함)	              |      템플릿을 기반으로 인스턴스 생성 시 추가 설정 변경 가능 |
|주요 목적	          |  인스턴스의 소프트웨어 환경(운영 체제 및 앱)을 미리 구성해두는 용도	|    인스턴스를 시작하는 데 필요한 모든 설정을 템플릿화하고 관리하는 용도 |


> `키 페어`

SuperKola-Key ( 생성한 키 페어 )

--------------------------------------------------------------------------------------

##  IAM - Identity and Access Management(IAM)

##  Cloud Watch

##  Cloud Front

## S3
Amazon S3  버킷  server.superkolatactics.io