﻿using Common.Commands;
using Common.Log;
using Common.Network.ServerManaging;
using Common.Network.Session;
using Common.Performance.PerformanceCounters;
using Common.Setting;
using Common.Util.Extensions;
using MySqlX.XDevAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCommon.CommandNodes.ServerNodes;

public class ServerSubNode_State : SubCommandNode
{
    public override string CommandText => "state";

    public ServerSubNode_State(BaseCommandNode commandNode) : base(commandNode)
    {
        Action = Task (command) =>
        {
            Logger.InfoLog($"-------------------------------------");

            Logger.InfoLog($"IsService : {ServerManager.Instance.IsService}, IsMaintenance : {ServerManager.Instance.IsMaintenance}, Data : {ServerManager.InformationETC.Maintenance.ToJson()} ");

            Logger.InfoLog($"-------------------------------------");

            ThreadPool.GetMinThreads(out int minWorkerThreads, out int minIoThreads);
            ThreadPool.GetMaxThreads(out int maxWorkerThreads, out int maxIoThreads);
            ThreadPool.GetAvailableThreads(out int availableWorkerThreads, out int availableIoThreads);

            Logger.InfoLog($"ThreadPool - min :{minWorkerThreads},{minIoThreads} max : {maxWorkerThreads},{maxIoThreads} available : {availableWorkerThreads},{availableIoThreads} ");
            Logger.InfoLog($"ThreadPool - ThreadCount {ThreadPool.ThreadCount}, CompletedWorkItemCount {ThreadPool.CompletedWorkItemCount} PendingWorkItemCount {ThreadPool.PendingWorkItemCount} ");
            

            Logger.InfoLog($"-------------------------------------");

            var sessions = SessionManager.Instance.GetAllServerSessions();
            foreach (var session in sessions)
            {
                Logger.InfoLog($"Linked Role:{session.SessionType,-12} Idx:{session.Index}, remote:{session.JobSocket?.Socket.RemoteEndPoint} local:{session.JobSocket?.Socket.LocalEndPoint}");
            }

            Logger.InfoLog($"-------------------------------------");

            Logger.InfoLog($"UserCount:{SessionManager.Instance.UserSessionCount}");

            Logger.InfoLog($"-------------------------------------");

            return Task.CompletedTask;
        };
    }
}

