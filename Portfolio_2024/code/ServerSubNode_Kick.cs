﻿using Common.Commands;
using Common.Network.Session;
using IDL.FlatMessages;

namespace GameCommon.CommandNodes.ServerNodes;

internal sealed class ServerSubNode_Kick : SubCommandNode
{
    public override string CommandText => "kick";

    public ServerSubNode_Kick(BaseCommandNode commandNode) : base(commandNode)
    {
        Action = async (command) =>
        {
            var usableSessionTypes = SessionFactory.GetUsableSessionTypes();

            if (command.Length != 1)
            {
                CommandManager.ShowSubCommandList(usableSessionTypes.Select(x => x.ToString()));
                return;
            }

            ESessionType? tempSessionType = null;
            foreach (var type in Enum.GetValues<ESessionType>())
            {
                if (type.ToString().ToLower() != command[0].ToLower())
                    continue;

                tempSessionType = type;
                break;
            }

            if (tempSessionType == null)
            {
                CommandManager.ShowSubCommandList(usableSessionTypes.Select(x => x.ToString()));
                return;
            }

            var sessionType = (ESessionType)tempSessionType;
            if (usableSessionTypes.Contains(sessionType) == false)
            {
                CommandManager.ShowSubCommandList(usableSessionTypes.Select(x => x.ToString()));
                return;
            }

            switch (sessionType)
            {
                case ESessionType.User:
                    await SessionManager.Instance.KickAllUserAsync(EResultType.LogoutCommand);
                    break;

                default:
                    {
                        var sessions = SessionManager.Instance.GetServerSessionsByType(sessionType);
                        if (sessions == null)
                            break;

                        foreach (var session in sessions)
                        {
                            SessionManager.Kick(session, IDL.FlatMessages.EResultType.LogoutCommand);
                        }
                    }
                    break;
            }
        };
    }
}
