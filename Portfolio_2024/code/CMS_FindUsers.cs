﻿using Common.Database;
using Common.Log;
using Common.Network.Session;
using Common.Util;
using GameCommon.Entity.Account;
using GameCommon.Entity.Game;
using GameCommon.Models.Web;
using GameCommon.Redis;
using Google.FlatBuffers;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Data;
using WebServer.Utils;
using static WebServer.CMS.CMS_FindUsers.Response;

namespace WebServer.CMS
{
    public class CMS_FindUsers
    {
        public class Request
        {
            public string nickname { get; set; } = string.Empty;
            public string uid { get; set; }
            public string ip { get; set; } = string.Empty;
            public long userIdx { get; set; } = 0;
            public int offset { get; set; } = 0;
            public int limit { get; set; } = 20;
        }
        public class Response
        {
            public class Data : Account
            {
                public string uid { get; set; } = string.Empty;

                // db column name
                // -- User DB
                public long userIdx { get; set; }
                public string nickname { get; set; } = string.Empty;

                // -- game server 
                public long connectServerIdx { get; set; } // 접속 상태 , 접속한 서버 인덱스를 넘긴다.

                public string note { get; set; } = string.Empty;
            }

            public List<Data> result { get; set; } = new List<Data>();
        }
    }

    public static partial class Handler
    {
        public static async Task DoWork(CMS_FindUsers.Request request, HttpResponse httpResponse)
        {
            //---------------------------------------------------------------------------
            // user, acccount  join

            string accountTable = Account.GetDBName();
            string userTable = User.GetDBName();

            using var wrapperRoot = await DatabaseManager.Instance.GetDisposableConnectionAsync(EDatabaseType.Root);
            var connectionRoot = wrapperRoot.Value;

            string userQuery = "";
            if ((request.uid == string.Empty) && (request.nickname == string.Empty) && (request.ip == string.Empty) && (0 >= request.userIdx))
            {                
                // all
                userQuery = $"SELECT * FROM {userTable} LEFT JOIN {accountTable} ON {userTable}.accIdx={accountTable}.accIdx LIMIT {request.limit} OFFSET {request.offset}";
            }
            else
            {
                userQuery = $@"SELECT * FROM {userTable} us LEFT JOIN {accountTable} ac ON us.accIdx = ac.accIdx 
WHERE (ac.accessToken = '{request.uid}' OR '' = '{request.uid}') AND 
(us.nickname = '{request.nickname}' OR '' = '{request.nickname}') AND 
(ac.lastLogInIP = '{request.ip}' OR '' = '{request.ip}') AND 
(us.userIdx = {request.userIdx} OR 0 >= {request.userIdx}) LIMIT {request.limit} OFFSET {request.offset}";
            }

            DataTable? userDataTable = await connectionRoot.ExecuteQueryAsync(userQuery, new MySqlParameter[] { });

            //---------------------------------------------------------------------------
            // data row filter

            DataRowFilter memberRow = new DataRowFilter(userDataTable);

            CMS_FindUsers.Response response = new CMS_FindUsers.Response();
            
            response.result = memberRow.ToList<Data>();

            //---------------------------------------------------------------------------
            // 유저 접속 확인 

            // redis 
            WA_UserLogON.Request RequestUserLogON = new WA_UserLogON.Request();            

            for(int i = 0; i < response.result.Count; i++)
            {
                var data = response.result[i];
                
                data.connectServerIdx = 0;

                var getUserSession = await RedisUser.GetSessionInfo(data.accIdx);

                if (getUserSession == null)
                {
                    continue;
                }
                if (getUserSession.connectedServer == 0 ||
                    getUserSession.connectedServer == (int)ESessionType.LoadBalance ||
                    getUserSession.connectedServer != (int)ESessionType.Game )
                {   
                    continue;
                }

                data.connectServerIdx = getUserSession.gameServerId;

                //
                RequestUserLogON.accessTokens.Add(data.accessToken);
                RequestUserLogON.accIdx.Add(data.accIdx);
                RequestUserLogON.gameServerId.Add(data.connectServerIdx);
            }
                        
            // loadbalance server logon check
            if (response.result.Count > 0 && RequestUserLogON.accessTokens.Count > 0)
            {
                await AdminWebApi.Request(WA_UserLogON.Uri,
                RequestUserLogON,
                (WA_UserLogON.Response res) =>
                {
                    for (int i = 0; i < res.accessTokens.Count; i++)
                    {
                        CMS_FindUsers.Response.Data? responseData = response.result.Find((value) => value.accessToken == res.accessTokens[i]);
                        if (responseData == null)
                        {
                            continue;
                        }

                        if (i >= res.serverIdxes.Count) // 인덱스가 다른 경우 
                        {
                            responseData.note = $"Error - Index Length : {res.serverIdxes.Count} , i : {i} , res.accessTokens.Count : {res.accessTokens.Count}";
                            Logger.ErrorLog(responseData.note);
                            continue;
                        }

                        if (false == responseData.accessToken.Equals(res.accessTokens[i]))
                        {
                            responseData.note = $"Error - uid : responseData {responseData.accessToken}, res.accessTokens : {res.accessTokens[i]}";
                            Logger.ErrorLog(responseData.note);
                            continue;
                        }

                        if (responseData.connectServerIdx != res.serverIdxes[i]) // 서버 id 가 다른경우
                        {
                            responseData.note = $"Error - Redis serverIdx : {responseData.connectServerIdx} , WA_UserLogON serverIdx : {res.serverIdxes[i]}";
                            Logger.ErrorLog(responseData.note);
                            continue;
                        }
                    }
                });
            }

            //---------------------------------------------------------------------------
            // result 

            JObject all = JObject.Parse(response.ToCamelJson());
            JArray array = (JArray)all["result"];

            foreach (JObject item in array)
            {
                item.Remove("accIdx");
                item.Remove("accessToken");
                item.Remove("whiteList");
                item.Remove("databaseType");
                item.Remove("isDirty");
            }

            await httpResponse.WriteAsync(all.ToString());
        }
    }
}
