﻿using Common.Architecture.RankAggregators;
using Common.Network.Session;
using FlatMessages.ServerMessages;
using GameCommon.Session;
using GameCommon.Statistics;
using LoadBalanceServer.Session;
using MaxMind.GeoIP2.Model;
using MySqlX.XDevAPI;
using Org.BouncyCastle.Ocsp;
using System.Collections.Concurrent;
using System.Runtime.CompilerServices;

namespace LoadBalanceServer;

public static class LoadBalancer
{
    private static int BalancingCount = CommonDefine.ConnectProcessCount;

    private static readonly RankAggregatorByCount<GameSession> RankAggregator = new(BalancingCount, true);
    private static readonly ConcurrentDictionary<string, long> LoadBalancedIds = new();
    
    // UserId, CountryCode
    private static readonly ConcurrentDictionary<string, ConcurrentQueue<string>> CountryCodeById = new();
    public static void AddCountryCodeById(string Id, string countryCode)
    {
        ConcurrentQueue<string> queue = null;
        CountryCodeById.TryGetValue(Id, out queue);
        if(queue == null)
        {
            queue = new ConcurrentQueue<string>();
            queue.Enqueue(countryCode);
            CountryCodeById.TryAdd(Id, queue);
        }
        else
        {
            queue.Enqueue(countryCode);
        }
    }
    public static void RemoveCountryCodeById(string Id)
    {
        ConcurrentQueue<string> queue = null;
        CountryCodeById.TryGetValue(Id, out queue);
        if(queue != null) 
        {
            string countryCoude = string.Empty;
            if(queue.TryDequeue(out countryCoude) == false)
            {
                Logger.ErrorLog($"Fail Remove Country Code : {countryCoude}");
            }
            if(queue.Count() <= 0)
            {
                CountryCodeById.TryRemove(Id, out _);
            }
        }
    }
    public static void AddGameServer(GameSession session)
    {
        DebugLog($"GameServerId:{session.Id}");
        RankAggregator.AddTarget(session.RankTarget);
    }
    // 
    public static GameSession? GetGameServer() => RankAggregator.GetTarget();
    public static List<GameSession?> GetGameSessions() => RankAggregator.GetList();
    public static void RemoveGameServer(GameSession session)
    {
        DebugLog($"GameServerId:{session.Id}");
        RankAggregator.PostRemoveTarget(session.RankTarget);
    }

    public static GameSession? GetLoadBalancedServer(string id)
    {
        if (LoadBalancedIds.TryGetValue(id, out var gameServerIdx) == false)
        {
            DebugLog($"UserId:{id}");
        }
        else
        {
            DebugLog($"UserId:{id}, GameServerIdx:{gameServerIdx}");
        }

        return SessionManager.Instance.GetServerSession(ESessionType.Game, gameServerIdx) as GameSession;
    }
    
    public static int RemainUserCount(GameSession session)
    {
        int userCount = RankAggregator.GetCount(session.RankTarget);
        if(userCount >= CommonDefine.MaxUserCount)
        {
            Logger.ErrorLog($"Game Server User Count Pool, SessionId : {session.Index},  UserCount:{userCount}", false);
            return 0;
        }
        else if(userCount < 0)
        {
            Logger.ErrorLog($"Game Server User Count Pool Error , SessionId : {session.Index},  UserCount:{userCount}", false);
            userCount = 0;
        }

        return CommonDefine.MaxUserCount - userCount;
    }


    public static void IncreseGameServerCount(string id, GameSession gameSession)
    {
        gameSession.AddId(id);
        //RankAggregator.AccumulateIncreaseValue(gameSession.RankTarget);
    }
    public static void Sorting()
    {
        RankAggregator.Sorting();
    }

    public static void RemoveLoadBalancedId(GameSession session, string id, [CallerMemberName] string memberName = "")
    {   
        DebugLog($"UserId:{id}, GameServerIdx:{session.Index}");

        if (session.RemoveId(id) == true)
        {
            //RankAggregator.DescreaseValue(session.RankTarget, memberName);
        }

        RemoveCountryCodeById(id);

    }

    private static void DebugLog(string text, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "")
    {
#if DEBUG
        Logger.DebugLog(EDebugLogType.LoadBalancing, $"{memberName} - {text}");
#endif
    }

    public static ConcurrentDictionary<string, ConcurrentQueue<string>> CloneCountryCodes() => CountryCodeById;
}
