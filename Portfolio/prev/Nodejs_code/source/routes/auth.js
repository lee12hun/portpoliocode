var express = require('express');

var router = express.Router();

///////////////////////////////////////////////////////////////
// 변경된 

var methodOverride = require('method-override');
var session = require('express-session');


///////////////////////////////////////////////////////////////
// passport

var passport = require('passport');
var FacebookStrategy = require('passport-facebook').Strategy;

// serialize
// 인증후 사용자 정보를 세션에 저장
passport.serializeUser(function(user, done) {
    console.log('serialize');
    done(null, user);
});

// deserialize
// 인증후, 사용자 정보를 세션에서 읽어서 request.user에 저장
passport.deserializeUser(function(user, done) {
    //findById(id, function (err, user) {
    console.log('deserialize');
    done(null, user);
    //});
});

// test_02_02              
// clientID: '394816140691334'
// clientSecret: '1be44b246fbbdbdcbd05f52d049a5c84' 
// callbackURL: "http://52.69.245.57:3000/auth/facebook/callback"

passport.use(new FacebookStrategy({
        clientID: '394816140691334',
        clientSecret: '1be44b246fbbdbdcbd05f52d049a5c84',
        callbackURL: "http://52.69.245.57:3000/auth/facebook/callback"
    },
    function(accessToken, refreshToken, profile, done) {
        console.log(profile);
        done(null,profile);
    }
));

///////////////////////////////////////////////////////////////
// use 

router.use(methodOverride());//app.use(express.methodOverride());
router.use(session({ secret: 'your secret here' })); //app.use(express.session({ secret: 'your secret here' }));
router.use(passport.initialize());
router.use(passport.session());


///////////////////////////////////////////////////////////////
// url

router.get('/', function(req, res, next) {
  //res.render('index', { title: 'Express' });
  res.send('respond with a resource');

  
  //res.render('facebooklogin', { title: 'Express' });
  //res.render('facebooklogin02', { title: 'Express' });
  //res.render('facebooklogin03', { title: 'Express' });
});


router.get('/auth/facebook', passport.authenticate('facebook'));
router.get('/auth/facebook/callback',
    passport.authenticate('facebook', { successRedirect: '/login_success',
        failureRedirect: '/login_fail' }));
router.get('/login_success', ensureAuthenticated, function(req, res){
    res.send(req.user);
});
router.get('/logout', function(req, res){
    req.logout();
    res.redirect('/');
});
function ensureAuthenticated(req, res, next) {
    // 로그인이 되어 있으면, 다음 파이프라인으로 진행
    if (req.isAuthenticated()) { return next(); }
    // 로그인이 안되어 있으면, login 페이지로 진행
    res.redirect('/');
}





module.exports = router;
