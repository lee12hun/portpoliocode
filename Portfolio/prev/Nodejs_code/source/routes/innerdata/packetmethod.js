
'use strict';

// var logobj = {
// 	typelog, reqQuery, res, err, pkData
// };


exports.logObject = function (typelog, reqQuery, res, err, pkData){
	this.typeLog = typelog;
	this.reqQuery = reqQuery;
	this.res = res;
	this.err = err;
	this.pkdata = pkData;
};

function sendJSON ( log, res, item ){
	console.log(log , item )
	res.writeHead(200,{'Content-Type':'application/json'});
	res.end( JSON.stringify( item ) );
	//res.json( item );
}

exports.sendJSON = sendJSON;

//exports.checkErrData = function ( obj ){
exports.checkErrData = function ( typelog, reqQuery, res, err, pkData ){

	if(err){
		sendJSON("Error Page", res, {"RESULT":false , "CONTENTS" :{ "MSG_TYPE":typelog , "Error_Database":err} });
		return false; // error
	}
	else if( pkData == null && pkData == undefined ){// json data error 		
		sendJSON("Error Page", res, {"RESULT":false , "CONTENTS" :{ "MSG_TYPE":typelog , "Error_ClientQuery":reqQuery} });
		return false; // error
	}	
	/*if(obj.err){
		sendJSON("Error Page", obj.res, {"RESULT":false , "CONTENTS" :{ "MSG_TYPE":obj.typeLog , "Error_Database":obj.err} });
		return false; // error
	}
	else if( obj.pkData == null && obj.pkData == undefined ){// json data error 		
		sendJSON("Error Page", obj.res, {"RESULT":false , "CONTENTS" :{ "MSG_TYPE":obj.typeLog , "Error_ClientQuery":obj.reqQuery} });
		return false; // error
	}	*/
	return true; // success 
};

exports.checkErr = function (typelog, res, err ){

	if( err ){
		sendJSON("Error Page", res, {"RESULT":false , "CONTENTS" :{ "MSG_TYPE":typelog , "Error_Database":err} });
		return false; // error
	}
	return true; // success 
};


exports.success = function (typelog, res, pkData){

	if( pkData ){
		sendJSON( "Success Page", res, {"RESULT":true , "CONTENTS":pkData , "MSG_TYPE":typelog } );	
	}
	else{
		sendJSON( "Success Page", res, {"RESULT":true , "MSG_TYPE":typelog} );		
	}
	return true;
};