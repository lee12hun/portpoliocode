
// MySQL
var mysql = require('mysql');

var config = require('../config/databaseconfig.js');

var pool = mysql.createPool( config.mysql );

var testWrite = function( obj ){
	pool.getConnection(function(err, conn){
		if(err) throw err;

		var sql = "insert into "+ config.mysql.database +" SET?";

		conn.query( sql, obj, function(err, rows){
			if( err ){
				console.error('insert error', err);
				console.error('insert error obj ', obj);
				//res.sendStatus(400);
			}else{
				//console.log('Success');
				//res.redirect('/movies');
			}
			conn.release();
		});
	});
}

exports.message = function ( userid, log, location ){

	var obj = {
		userid : userid
		log : log,
		location : location
	}

	testWrite( obj );
}