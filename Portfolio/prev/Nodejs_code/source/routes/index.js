var express = require('express');
var async = require('async');

var db = require('./innerdata/mongodb_database.js');
var packet = require('./innerdata/packetmethod.js');

var router = express.Router();


/*
	
{
    "user_id":"testID",
    "user_pw":"1234"	
}

*/
router.post('/login', function(req, res, next) {

	db.SignUp.findOne(req.body, function( err, data){
		console.log( err );
		console.log( data );

		//if( packet.checkErrData( new packet.logObject("login", req.body, res, err, data) ) ){
		if( packet.checkErrData( "login", req.body, res, err, data ) ){

			packet.success("login", res );
		}
	});
});

/*

{
    "user_id":"testID12",
    "user_pw":"1234",
    "car_name":"SONATA",
    "car_career":100
}

*/
router.post('/signup', function(req, res, next) {

	var duplicationcheck = {
		user_id : req.body.user_id
	}

	db.SignUp.findOne(duplicationcheck, function( err, data){
		console.log( err );
		console.log( data );

		if( packet.checkErr("signup", err ) ){

			if( data == null || data == undefined ){ // users already exists with user_id

				db.SignUp.create( req.body , function( err, item ){

					if( packet.checkErrData("signup", req.body, res, err, item ) ){

						packet.success("signup", res );
					}
				});
			}else{				
				sendJSON("Error Page", res, {"RESULT":false , "CONTENTS" :{ "Type":"signup" , "Error_DuplicationID":duplicationcheck} });
			}		
		}
	});	
});


router.get('/page/:page_number', function(req, res, next)
 {

	var page_package = {	
		_id :0,	//page_uid :0,	// database error
		page_number : 1,
		user_id :1,
		title :1,
		contents : 1,
		img_path:1,
		replylist:1
	};	

	// find 1 type
	db.PageList.findOne( req.params , page_package , function( err, pkData ){

		if( packet.checkErrData("page ", req.params, res, err, pkData ) ){

			packet.success("page ", res, pkData );
		}
	});

	// find 2 type
	//db.PageList.findOne( req.params ).select(page_package).exec( function(err,data){
});

/*	client protocol

{
    "user_id" : "testID12",
    "title" : "ABCBDBREEE1234title",
    "contents" : "contnjdkfjklaeji12jkljklsdjilwe23jkl",
    "img_path" : "ajkl.jpeg"
}

*/

// delete 		/page/write   
// new Page url /page/write/upload

// router.post('/page/write', function(req, res, next){

// 	console.log(req.body);

// 	db.PageList.create( req.body , function( err, item ){
		
// 		if( packet.checkErrData("page write", req.body, res, err, item ) ){

// 			packet.success("page write", res );
// 			//sendJSON( "Success Page", res, {"RESULT":true} );
// 		}
// 	});	
// });



// form-data type
//router.post('/page/write/upload', filemanager.fileupload );



/*	client protocol
	
	page_uid  // page_number

{
    "page_number":0,
    "title" : "ABCBDBREEE1234title",
    "contents" : "contnjdkfjklaeji12jkljklsdjilwe23jkl",
    "img_path" : "ajkl.jpeg"
}

*/
router.post('/page/fix', function(req, res, next) {

	console.log(req.body);

	db.PageList.findOneAndUpdate( {page_number:req.body.page_number}, req.body, function(err, data, raw){
    	
		if( packet.checkErrData("page fix", req.body, res, err, data ) ){

			packet.success("page fix", res );
		}
	});

	/*	//문제점   doc.save() Error 만들어진 객체가 조건의 만족하는 객체가 생성되어서 save() 함수가 없다.  ( 위 findOneAndUpdate 로 변경 함  )
	db.PageList
	.findOne(req.body.page_number)//.findOne(req.body.page_uid)
	.select(req.body)
	.exec(function(err,doc){
		if(err){
			console.log(err);
		}
		else{		
			doc = req.body;			
			doc.save(function(err){  
				if(err){
					console.log("ERR" , err );
				}
				else{
					sendJSON()
				}
			});
		}				
	});	*/
});


/*	client protocol
	
	signup_uid // page_number

{
    "page_number" : 5,
    "user_id" : "djsfhjlae@test.com"
}
*/
router.post('/page/terminate', function(req, res, next) {

	console.log(req.body);

	db.PageList
	.findOne({ page_number: req.body.page_number , user_id : req.body.user_id })
	.exec(function(err,doc){
		
		if( packet.checkErrData("page terminate", req.body, res, err, doc ) ){

			doc.remove(function(removeErr){

				if( packet.checkErr("page terminate", res, err ) ){

					packet.success("page terminate", res );					
				}
			});
		}	
	});
});


/*	client protocol

	page_uid // page_number

{
    "page_number" : 0,
    "replylist" : {
        "user_id" : "FixId@test.com",
        "contents" : "hahahahahahah!"
    }
}

*/

router.post('/reply/write', function(req, res, next) {

	console.log(req.body);

	db.PageList
	.findOne({ page_number: req.body.page_number })
	.exec(function(err,doc){

		if( packet.checkErrData("reply write", req.body, res, err, doc ) ){

			// 댓글별 auto _increment 추가 
			var replyObject = req.body.replylist;
			replyObject.reply_number = doc.replyincrement++;
			doc.replylist.push(replyObject);

			console.log("replyObject : ",replyObject);

			doc.save(function(save_err){

				console.log( "errerrerr" ,save_err );

				if( packet.checkErr("reply write", res, save_err ) ){

					packet.success("reply write", res );
				}
			});
		}	
	});
});

/*  DELETE
	page_uid  // page_number

{
    "page_number" : "0",
    "replylist" : {
        "reply_number" : "1"
    }
}

*/

router.post('/reply/terminate', function(req, res, next) {

	console.log(req.body);	
	
	db.PageList
	.findOne({ page_number: req.body.page_number })
	.exec(function(err,doc){
		if( packet.checkErrData("reply terminate", req.body, res, err, doc ) ){
			
			var index = 0;
			for( ; index < doc.replylist.length ; index++)
			{
				if( req.body.replylist.reply_number == doc.replylist[index].reply_number ){

					doc.replylist.splice(index,1);
					//delete doc.replylist[index];
					break;
				}
			}
			if( index == doc.replylist.length ){
				// nothing delete item
				sendJSON("Error Page", res, {"RESULT":false , "CONTENTS" :{ "Type":"reply terminate" , "Error_nothingItem":req.body} });
			}
			else{
				doc.save( function(err){

					if( packet.checkErr("reply terminate", err ) ){

						packet.success("reply terminate", res );
					}
				});
			}			
		}		
	});
});

// 검색어 리스트 전달 
/*  	

{
    "keyword":"",
    "page_count":4
}

*/
router.get('/search', function(req, res, next) {

	console.log("search");

	var query = req.query;
	//var query = req.body;

	async.series(
	[
		//Database  Total Count 
		function(callback) {
			var finder = {};
			var selecter = {'_id':false};	
			var order = { sort:{ updated: -1 } };//Sort by Date Added DESC 			

			if( query.keyword )
			{
				finder = {"title": {'$regex':query.keyword}};

				console.log("finder", finder);
			}
			else{
				console.log("finder_", finder);
			}

			db.PageList.find( finder,selecter,order)
			.count(function(err,page_totalcount){
				if( packet.checkErrData("search t", query, res, err, page_totalcount ) ){

					var packagedata = {
						totalcount : page_totalcount
					};					
					//packet.success("search t", res, packagedata );
					callback(null,packagedata);
				}		    	
		    });			
		}
	], function(err, results){
		if ( err ) {
					console.log('Error', err);
					res.sendStatus(500);	// Internal Server Error!
		}
		else {
			console.log("results ",results);			

			var finder = {};		
			if( query.keyword )
			{
				finder = {"title": {'$regex':query.keyword}};
			}

			// selecter 
			var selecter = {	
				_id :0,					
				page_number :1,
				title:1,
				user_id :1,
				contents :1,
				thumbnail_path :1
			};
			
			// 정렬 	
			var order = {
				skip:parseInt(query.page_count), // Starting Row
				limit:5, // Ending Row
			 	sort:{
			 	 	updated: -1 //Sort by Date Added DESC 
			 	}
			};

			db.PageList.find( finder, selecter, order, function(err,pk){

				if( packet.checkErrData("search", query, res, err, pk ) ){

					console.log( "pk : " , pk );
					console.log( "results[0] : totalcount : " , results[0] );

					packet.success("search", res, pk );
				}
			});
		}		
	});
});

module.exports = router;