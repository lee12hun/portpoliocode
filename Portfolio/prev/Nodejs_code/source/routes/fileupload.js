var express = require('express');
var formidable = require('formidable');
var fs = require('fs');
var fse = require('fs-extra');

var easyimg = require('easyimage');

var pathUtil = require('path');
var async = require('async');

var db = require('./innerdata/mongodb_database.js');
var packet = require('./innerdata/packetmethod.js');


/////////////////////////////////////////////////////////////////////////

var uploadDir = __dirname + '/upload';
var thumbnailDir = __dirname + '/thumbnail';

if( !(fs.existsSync(uploadDir) && fs.existsSync(thumbnailDir) ) ){
	console.error('upload, thumbnail 폴더 없음');
	process.exit();
}

/////////////////////////////////////////////////////////////////////////

var AWS = require('aws-sdk');
AWS.config.region = "ap-northeast-1";
AWS.config.accessKeyId = "AKIAI7OVYRRKVFL5LUUA";
AWS.config.secretAccessKey = "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";

/////////////////////////////////////////////////////////////////////////

var s3 = new AWS.S3();

console.log( 'href', s3.endpoint.href );

var bucketName = "leestore";

/////////////////////////////////////////////////////////////////////////

//var resources = [];

var router = express.Router();


fse.emptyDirSync( uploadDir );
fse.emptyDirSync( thumbnailDir );


/*

var queueImage = [];
var queuethumbnailImage = [];

////////////////////////////////////////////
// Queue  쌓아 놓고 지워도 문제가 생기고 
// 업로드 입력을 받은후 10초후에 지워도 문제가 생긴다.
// 문제분석] 업로드된 파일을 메모리에 캐쉬하고 있는듯 하다 ,그래서 지우면 문제가 생긴다.
// 에러는 항상 unlink 오류가 난다.

var timer;
var myVar;
function resetInterval() {
    console.log('resetInterval ');

     clearTimeout(myVar);

     myVar=setTimeout( function(){
        console.log('Hello');
        fse.emptyDir( uploadDir );
		fse.emptyDir( thumbnailDir );

    },5000);

     // clearInterval(timer);
     // timer = setInterval( function ()  
     // {
     //    console.log("restarted interval");
     // },
     // 5000); 
 }
*/

router.post('/page/write/upload',function (req, res){
	
	var form = new formidable.IncomingForm();
	console.log("about to parse");

	form.keepExtensions = true;
	form.uploadDir = uploadDir;
	//form.multiples = true;  // 멀티 업로드 

	form.parse( req, function( err, fields, files ){

		if( err ){

			console.log( "form.parse error ",err );

			packet.sendJSON("Error Page", res, {"RESULT":false , "CONTENTS" :{ "MSG_TYPE":"page write/upload" , "Error_empty Text":err} });

			return;
		}

		if( fields == undefined ||
			fields.user_id == "" ||
			fields.title == "" ||
			fields.contents == "" )
		{

			console.log( "page/write/upload , input text string , error ",err );

			packet.sendJSON("Error Page", res, {"RESULT":false , "CONTENTS" :{ "MSG_TYPE":"page write/upload" , "Error_empty Text":fields} });
			
			return;
		}
		
		if( files == undefined || files.img_path === undefined )
		{
			// 정보 저장
			var info = fields;			
			info.img_path = "";
			info.thumbnail_path = "";

			console.log("not image file , input text : ", info);

			db.PageList.create( info , function( err, item ){
				
				if( packet.checkErrData("page write", info, res, err, item ) ){

					packet.success("page write", res );
					//sendJSON( "Success Page", res, {"RESULT":true} );
				}				
			});

			return;
		}


		var now = new Date();
		var newFileName = 'poster_' + now.getFullYear() + now.getMonth() + now.getDate() + now.getHours() + now.getMinutes() + now.getSeconds();
		console.log("newFileName  " + newFileName);

		console.log( "files.img_path ",files.img_path );

		var fileName = files.img_path.name;
		var tempFilePath = files.img_path.path;
		var contentType = files.img_path.type;
		var thumbnailFilePath = thumbnailDir + pathUtil.sep + newFileName + fileName;

		////////////////////////////////////////////////		
		// fs.rename(files.img_path.path, files.img_path.name , function(err) {
		//   if (err) {
		//     //fs.unlink(files.img_path.name);
		//     //fs.rename(files.upload.path, "/tmp/test.png");
		//   }
		//  // fs.unlink(files.img_path.name);

		// 	console.log( "files.img_path.name :", files.img_path.name );
		// 	console.log( "files.img_path.path :", files.img_path.path );

		//   console.log( "count :", count++ );
		// });


		console.log('start easyimg.thumbnail ' );
		easyimg.thumbnail({
			src:tempFilePath,
			dst:thumbnailFilePath,
			width:100
		}).then(function(image){
			///////////////////////////////////////////////////////////////////////////
			// 썸네일 이미지 성공 

			console.log('thumbnail created : ' , image);
			//callback(null, null);			

			///////////////////////////////////////////////////////////////////////////
			// S3에 파일 올리기 

			async.series(
			[
				function(callback) {

					// 파일 스트림 생성
					var imageStream = fs.createReadStream(tempFilePath);
					console.log("imageStream " + imageStream);
									 //fs.createReadStream(tempFilePath,{start:pos[0],end:pos[1]}).pipe(res);
									
					imageStream.on('error', function (err) {
						console.log( "imageStream.on error " );
						  if (err)  {
						  	//  throw err; 
						  	res.writeHead(200, {"Content-Type": "text/html"});
							res.write("thumbnail created:<br/>");
							res.write("<img src='/show' />");
							res.end();
							callback(err, null);
						}
					}); 
			
					imageStream.on('open', function () {
						console.log( "imageStream.on open " );					
						
						var extname = pathUtil.extname( fileName ); //console.log("extname  " + extname);
						var itemKey = "image/" + fields['user_id'] + "/" + newFileName + extname; //console.log("itemKey  ",itemKey);

						var params = {
						  Bucket: bucketName, 	// 필수
						  Key: itemKey,			// 필수
						  ACL: 'public-read',
						  Body: imageStream,
						  ContentType:contentType
						};					
						
						// 썸네일 저장
						s3.putObject(params, function(err, data) {
							if ( err ) {
								console.error('S3 PutObject Error', err);
								callback(err, null);
							}
							else {
								// 접근 경로 - 2가지 방법
								var imageUrl = s3.endpoint.href + bucketName  + '/' + itemKey;
								var imageSignedUrl = s3.getSignedUrl('getObject', {Bucket: bucketName, Key: itemKey});
								callback(null, imageUrl);							
							}
						});

						//callback(null, tempFilePath); //test

					});

				},
				function(callback) {

					var thumbnailStream = fs.createReadStream(thumbnailFilePath);

					console.log("thumbnailStream " + thumbnailStream);
									
					thumbnailStream.on('error', function (err) {
						console.log( "thumbnailStream.on error " );
						  if (err)  {
						  	//  throw err; 
						  	res.writeHead(200, {"Content-Type": "text/html"});
							res.write("thumbnail created:<br/>");
							res.write("<img src='/show' />");
							res.end();

							callback(err, null);
						}
					}); // thumbnailStream.on
			
					thumbnailStream.on('open', function () {

						var extname = pathUtil.extname( fileName );
						var thumbnailKey = "image/" + fields['user_id'] + "/thumbnail_" + newFileName + extname;

						var thumbnailParams = {
							Bucket : bucketName,
							Key: thumbnailKey,
							ACL: 'public-read',
							Body: thumbnailStream,
							ContentType:contentType
						}

						// 썸네일 저장
						s3.putObject(thumbnailParams, function(err, data) {
							if ( err ) {
								console.error('Thumbnail Error', err);
								callback(err, null);
							}
							else {
								var url = s3.endpoint.href + bucketName  + '/' + thumbnailKey;
								callback(null, url);		
							}						
						}); // s3.putObject

						//callback(null, thumbnailFilePath); //test

					}); // thumbnailStream.on
				}
			],function(err, results) {

				if ( err ) {					
					console.log('Error 0 ', err[0] );
					console.log('Error 1 ', err[1] );
					res.sendStatus(500);	// Internal Server Error! 
				}
				else {				

					// 정보 저장
					var info = fields;

					//var obj  = fields
					info.img_path = results[0];
					info.thumbnail_path = results[1];

					console.log("info : ", info);

					db.PageList.create( info , function( err, item ){
						
						if( packet.checkErrData("page write", info, res, err, item ) ){

							packet.success("page write", res );
							//sendJSON( "Success Page", res, {"RESULT":true} );
						}
					});
				}
			});	
		}, function(err) {		
			///////////////////////////////////////////////////////////////////////////
			// 썸네일 이미지 오류 
			// S3에도 파일을 올리면 안된다 

			console.error('Thumbanil Create Error', err);
			//callback(err, null);

			console.log("tempFilePath Delete: ", tempFilePath );
			console.log("thumbnailFilePath Delete: ", thumbnailFilePath );
			// fs.unlink(tempFilePath);
			// fs.unlink(thumbnailFilePath);


			res.writeHead(200, {"Content-Type": "text/html"});
			res.write("Thumbanil Create Error:<br/>");
			res.write("<img src='/show' />");
			res.end();

			// fs.unlink(files.img_path.name);

		});

		console.log('end easyimg.thumbnail ' );
	}); // form.parse	
});

module.exports = router;



  /*
	var form = new formidable.IncomingForm();

	form.encoding = 'utf-8';
	form.uploadDir = uploadDir;
	form.multiples = true;
	form.keepExtensions = true;	

	fse.emptyDirSync( uploadDir );
	fse.emptyDirSync( thumbnailDir );

	form.parse( req, function( err, fields, files ){
		
		var title = fields.title;
		
		console.log( "----------------------------------------------------------");
		console.log( "fields : " , fields , Date.now() );
		console.log( "files.img_path : " , files.img_path);

		if( files.img_path == undefined ){
			console.error('image path Error ');
			return;
		}

		//console.log( "files : " , files);

		var fileName = files.img_path.name;
		var tempFilePath = files.img_path.path;
		var contentType = files.img_path.type;

		var thumbnailFilePath = thumbnailDir + pathUtil.sep + fileName;

		var extname = pathUtil.extname( fileName );

		console.log("extname ", extname );

		var now = new Date();
		var newFileName = 'poster_' + now.getFullYear() + now.getMonth() + now.getDate() + now.getHours() + now.getMinutes() + now.getSeconds();

		async.series(
			[
				function(callback) {
					console.log( " easyimg.thumbnail " );
					// 임시 파일에서 썸네일 생성		
					easyimg.thumbnail({
						src:tempFilePath,
						dst:thumbnailFilePath,
						width:100
					}).then(function(image){
						console.log('thumbnail created : ', image);
						callback(null, null);
					}, function(err) {						
						console.error('Thumbanil Create Error', err);
						callback(err, null);
					});					
				},
				function(callback) {
					console.log( " tempFilePath ", tempFilePath );
					// 파일 스트림 생성
					var readStream = fs.createReadStream(tempFilePath);
					
					// 버킷 내 객체 키 생성
					//var itemKey = 'poster/' + newFileName + extname;					
					//var itemKey = "image/" + fields['user_id'] + "/" + files['img_path']['name'];
					var itemKey = "image/" + fields['user_id'] + "/" + newFileName + extname;
					
					console.log("itemKey  ",itemKey);

					var params = {
					  Bucket: bucketName, // 필수
					  Key: itemKey,					// 필수
					  ACL: 'public-read',
					  Body: readStream,
					  ContentType:contentType
					}
					
					s3.putObject(params, function(err, data) {
						if ( err ) {
							console.error('S3 PutObject Error', err);
							callback(err, null);
						}
						else {
							// 접근 경로 - 2가지 방법
							var imageUrl = s3.endpoint.href + bucketName  + '/' + itemKey;
							var imageSignedUrl = s3.getSignedUrl('getObject', {Bucket: bucketName, Key: itemKey});
							callback(null, imageUrl);							
						}
					});										
				},
				function(callback) {

					console.log( " thumbnailFilePath ", thumbnailFilePath );

					// 썸네일 키 생성

					//var thumbnailKey = 'thumbnail/' + newFileName + extname;
					//var thumbnailKey = "image/" + fields['user_id'] + "/thumbnail_" + files['img_path']['name'];
					var thumbnailKey = "image/" + fields['user_id'] + "/thumbnail_" + newFileName + extname;

					var thumbnailBody = fs.createReadStream(thumbnailFilePath);
					var thumbnailParams = {
						Bucket : bucketName,
						Key: thumbnailKey,
						ACL: 'public-read',
						Body: thumbnailBody,
						ContentType:contentType
					}
					// 썸네일 저장
					s3.putObject(thumbnailParams, function(err, data) {
						if ( err ) {
							console.error('Thumbnail Error', err);
							callback(err, null);
						}
						else {
							var url = s3.endpoint.href + bucketName  + '/' + thumbnailKey;
							callback(null, url);		
						}						
					});					
				},
				function(callback) {

					console.log( " unlinkSync " );
					// 임시 파일 삭제
					//fs.unlinkSync(tempFilePath);
					//fs.unlinkSync(thumbnailFilePath);	
					fs.exists('thumbnail',function(exists){
		                if(exists ==true){
		                	fs.rmdir('thumbnail');
		                }
		                else{
		                   console.log('thumbnail  noFile');
		                }
		             });

					fs.exists('upload',function(exists){
		                if(exists ==true){
		                	fs.rmdir('upload');
		                }
		                else{
		                   console.log('thumbnail  noFile');
		                }
		             });

					callback(null, null);									
				}
								
			]
			,function(err, results) {
				if ( err ) {
					console.log('Error', err);
					res.sendStatus(500);	// Internal Server Error!					
				}
				else {
					console.log("results ",results);

					var imageUrl = results[1];
					var thumbnailUrl = results[2];
					
					// var info = {
					// 	user_id : fields['user_id'],
					// 	url : imageUrl,
					// 	thumbnail : thumbnailUrl					
					// };

					// 정보 저장
					var info = fields;

					//var obj  = fields
					info.img_path = results[1];
					info.thumbnail_path = results[2];

					console.log("info : ", info);

					db.PageList.create( info , function( err, item ){
						
						if( packet.checkErrData("page write", info, res, err, item ) ){

							packet.success("page write", res );
							//sendJSON( "Success Page", res, {"RESULT":true} );
						}
					});	

					//resources.push(info);

					//res.redirect('/');
					//res.send("success");
				}
			}
		);		
	});
	*/
