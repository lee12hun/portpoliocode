﻿using System.IO;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif


namespace L.EditorDefines
{
#if UNITY_EDITOR
    static public class FilePathEditor
    {
        static public string GetAssetPath(Object asset) // 현재 에세의 위치를 준다.
        {
            return AssetDatabase.GetAssetPath(asset);
        }

        static public string GetFolderPath(Object asset) // 에셋의 상위 폴더 위치를 준다.
        {
            string fullpath = AssetDatabase.GetAssetPath(asset);

            int endidx = fullpath.LastIndexOf(asset.name);
            string folderpath = fullpath.Substring(0, endidx);

            return folderpath;
        }

        // SearchOption.AllDirectories
        static public string[] GetFiles(Object path, string extension, SearchOption opt = SearchOption.TopDirectoryOnly)
        {
            string foldername = AssetDatabase.GetAssetPath(path);
            string[] files = Directory.GetFiles(foldername, extension, opt);

            for (int i = 0; i < files.Length; i++)
            {
                files[i] = files[i].Replace("\\", "/");
            }
            return files;
        }
    }

    static public class FileEditorTools
    {   
        public class TableElement
        {
            public string name;
            public L.FileDatas.CsvString.CSVTable loader;
            public TableElement(string n, L.FileDatas.CsvString.CSVTable l)
            {
                name = n;
                loader = l;
            }
        }
        
        [System.Serializable]
        public class FileFinder
        {
            public bool isRefesh;
            public Object resourceRoots; // Folder , Sprite ,Image ,Prefabs 
            public GameObject sceneObject;
            public List<TableElement> tempDatas = new List<TableElement>();
        }

        static public void SetFileFinder(FileFinder property)
        {
            if (GUI.changed && property.isRefesh)
            {
                property.isRefesh = false;
                
                string foldername = FilePathEditor.GetFolderPath(property.resourceRoots);

                string[] files = Directory.GetFiles(foldername, "*.csv");

                for (int f = 0; f < files.Length; f++)
                {
                    string objname = files[f].Replace(foldername, "").Replace(".csv", "");

                    GameObject n = new GameObject();
                    n.transform.name = objname;
                    n.transform.parent = property.sceneObject.transform;

                    property.tempDatas.Add(new TableElement(objname, L.FileDatas.CsvString.CSVTable.Load(files[f])));
                }
            }
        }
        /**/
        

        [System.Serializable]
        public class ProductSpriteData
        {
            public string name;
            public Sprite sprite;

            public ProductSpriteData(string n, Sprite s)
            {
                name = n;
                sprite = s;
            }
        }

        [System.Serializable]
        public class FileToCreateAsset
        {
            public bool isRefesh;
            public Object resourceRoots; // Folder , Sprite ,Image ,Prefabs 
            public GameObject sceneObject;
            public List<ProductSpriteData> images = new List<ProductSpriteData>();
        }

        static public void SetFileToCreateAsset(FileToCreateAsset property)
        {
            if (GUI.changed && property.isRefesh)
            {
                property.isRefesh = false;

                L.FileDatas.CsvString.CSVTable textable = new L.FileDatas.CsvString.CSVTable();

                textable = L.FileDatas.CsvString.CSVTable.Load(AssetDatabase.GetAssetPath(property.resourceRoots));

                int wpid = 0;// tables.GetColmn("wp_ID");
                int fithumbnail = 0;// tables.GetColmn("fi_thumbnail");

                for (int i = 0; i < textable.fieldNames[0].Count; i++)
                {
                    //if(textable.fieldNames[0][i].Equals("filename"))//fullname
                    if (textable.fieldNames[0][i].Equals("wp_ID"))//
                    {
                        wpid = i;
                        //break;
                    }
                    else if (textable.fieldNames[0][i].Equals("fi_thumbnail"))
                    {
                        fithumbnail = i;
                    }
                }


                //StringBuilder sz = new StringBuilder();
                for (int i = 0; i < textable.table.Count; i++)
                {
                    string numberid = textable.table[i][wpid];

                    string texturename = textable.table[i][fithumbnail].Replace("http://skonec11.cafe24.com/media/", "");

                    string paths = "Assets/Resources/" + texturename;                   

                    Sprite texsphere = (Sprite)AssetDatabase.LoadAssetAtPath(paths, typeof(Sprite));

                    if (texsphere != null)
                    {
                        property.images.Add(new ProductSpriteData(numberid, texsphere));
                    }
                }
            }
        }
    }
#endif
}