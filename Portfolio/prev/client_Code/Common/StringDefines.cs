﻿using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace L.StringDefines
{
    static public class GameString
    {
        private static StringBuilder localsz = new StringBuilder();

        public static string Format(string sz, object obj1)
        {
            localsz.Length = 0;
            return localsz.AppendFormat(sz, obj1).ToString();
        }
        public static string Format(string sz, object obj1, object obj2)
        {
            localsz.Length = 0;
            return localsz.AppendFormat(sz, obj1, obj2).ToString();
        }
        public static string Format(string sz, object obj1, object obj2, object obj3)
        {
            localsz.Length = 0;
            return localsz.AppendFormat(sz, obj1, obj2, obj3).ToString();
        }

        public static string DeleteClone(string data)
        {
            if (data.Contains("(Clone)"))
            {
                int startidx = data.LastIndexOf("(Clone)");
                string value = data.Substring(0, startidx);
                return value;
            }
            return data;
        }

        public static string FrontName(string data, string token = "_")
        {
            if (data.Contains(token))
            {
                int startidx = data.LastIndexOf(token);
                string value = data.Substring(0, startidx);
                return value;
            }
            return string.Empty; // return data; //
        }

        public static string BackNameReturn(string data)
        {
            string res = BackName(data);

            if (string.IsNullOrEmpty(res))
            {
                return data;
            }
            return res;
        }
        
        // 
        static public string VecToString(Vector3 vec)
        {
            return vec.ToString();
        }
        static public Vector3 StringToVec(string s)
        {
            string[] temp = s.Substring(1, s.Length - 2).Split(',');
            return new Vector3(float.Parse(temp[0]), float.Parse(temp[1]), float.Parse(temp[2]));
        }
    }
