﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using MEC;

namespace L.CoroutineDefines
{
    public class EventRoutine : DynamicStatic<EventRoutine>
    {
        
        public IEnumerator UIButtonUpDelay(System.Action action) 
        {
            yield return new WaitForFixedUpdate();// WaitForEndOfFrame();

            action();
        }

        public IEnumerator DelayAction(float time, System.Action action)
        {
            yield return new WaitForSeconds(time);

            action();
        }
    }

    public class MECRoutine : DynamicStatic<MECRoutine>
    {
        // NULL 체크
        CoroutineHandle? movetargetHandle;

        movetargetHandle = Timing.RunCoroutine(_SetTargetMove());

        if (movetargetHandle != null)
        {
            Timing.KillCoroutines(  movetargetHandle.Value  );
            movetargetHandle = null;
        }


        // SlowUpdate 
        IEnumerator<float> _UpdateUI()
        {            
            //Timing.Instance.TimeBetweenSlowUpdateCalls = 0.1f; // = 3f; run once 3seconds
            while (true)
            {
                //clock = Timing.LocalTime;

                UpdateBuild();

                yield return 0f;
            }
        }
        
        public IEnumerator<float> _MECUpdate(System.Action update, MonoBehaviour scr)
        {
            yield return Timing.WaitForOneFrame;

            while (scr.gameObject != null)
            {
                if (scr.gameObject.activeInHierarchy && scr.enabled)
                {
                    update();
                }

                yield return Timing.WaitForOneFrame;
            }
        }

        public void StopRoutineSafety(CoroutineHandle? handle)
        {
            if (handle != null)
            {
                Timing.KillCoroutines(handle.Value);
                handle = null;
            }
        }
    }
}
