﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace L.TransformDefines
{
    // 
    public class FindIncludeInActive<T> where T : Component
    {
        List<T> result = new List<T>();
        int index = 0;
        public FindIncludeInActive(Transform trans)
        {
            trans.GetComponentsInChildren<T>(true, result);
        }

        public T FindNext(string name)
        {
            T res = null;

            if(index < result.Count)
            {
                if (result[index].name.Equals(name) || result[index].name.Contains(name))
                {
                    res = result[index];
                }

                index++;
            }

            return res;
        }

        ~FindIncludeInActive()
        {
            result.Clear();
            index = -1;
        }
    }


    class Trans : DynamicStatic<Trans>
    {
        public Vector3 Vec3RotatedDirection(Quaternion v3Rotation, Vector3 v3Direction)
        {
            return v3Rotation * v3Direction;
        }

        // Target 방향으로 회전
        public Quaternion GetRotFromVectors(Vector3 target, Vector3 startPos)
        {
            return Quaternion.Euler(0, Mathf.Atan2(startPos.x - target.x, startPos.z - target.z) * Mathf.Rad2Deg, 0);
        }

        public float GetAngle(Vector3 vStart, Vector3 vEnd)
        {
            Vector3 v = vEnd - vStart;            
            return Mathf.Atan2(v.z, v.x) * Mathf.Rad2Deg;
        }
    }
}
