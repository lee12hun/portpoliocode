﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace L.PlatformDefines
{
    public class AndroidClass
    {
        private AndroidJavaObject activityContext;

        private AndroidJavaObject javaClassInstance;
        public AndroidJavaObject cudoObject
        {
            get { return javaClassInstance; }
        }

        private string path;
        bool m_bInit = false;

        public void Init()
        {
            using (AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            {
                activityContext = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
            }

            if (null == javaClassInstance)
            {
                javaClassInstance = new AndroidJavaObject("com.EasyMovieTexture.EasyMovieTexture");
                javaClassInstance.Call("setContext", activityContext);
                javaClassInstance.Call("CreatePlayer");
            }

            //javaClass = new AndroidJavaClass("com.EasyMovieTexture.EasyMovieTexture");
            //if (null != javaClass)
            //{
            //    javaClassInstance = javaClass.CallStatic<AndroidJavaObject>("CreateInstance");
            //    Debug.Log("DRMValue : " + javaClassInstance);
            //    if (null != javaClassInstance)
            //    {
            //        javaClassInstance.Call("setContext", activityContext);
            //        javaClassInstance.Call("CreatePlayer");
            //    }
            //}
            //else
            //{
            //    Debug.Log("DRMVAlue : JavaClass is Null");
            //}

            AndroidJavaClass jc = new AndroidJavaClass("android.os.Environment");
            path = jc.CallStatic<AndroidJavaObject>("getExternalStorageDirectory").Call<string>("getAbsolutePath");
            Debug.Log("DRMValue : Path" + path);
            if (false == m_bInit)
                InitPlayer();
            //AndroidJavaObject temp = new AndroidJavaClass("com.wustone.unityplugin.MyPluginActivity").CallStatic<AndroidJavaObject>("instance");
            //Debug.Log("LGUI : " + temp);
            //temp.Call("checkPermission");
            AndroidJavaClass tempClass = new AndroidJavaClass("android.support.v4.app.ActivityCompat");
            tempClass.CallStatic("requestPermissions", activityContext, new String[] { "android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE" }, 1);
            string path1 = path + "/drmplay/certificate";
            string path2 = path + "/drmplay/drmlog";
            string path3 = path + "/drmplay/debug";

            if (!Directory.Exists(path1))
                Directory.CreateDirectory(path1);
            if (!Directory.Exists(path2))
                Directory.CreateDirectory(path2);
            if (!Directory.Exists(path3))
                Directory.CreateDirectory(path3);

            //label.text = message;
        }

        public void InitPlayer()
        {
        }

    }
}
