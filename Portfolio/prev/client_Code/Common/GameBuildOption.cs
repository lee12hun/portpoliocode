﻿
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif


// 게임 별 빌드 옵션을 걸 파일을 따로 만든다. 테스트 예제

namespace L.GameBuildOption
{

    ///////////////////////////////////// GamePathOption

    static public class GamePathOption
    {
        public static bool DrawPathGizmo = true;
        public static float DrawPathGizmoSize = 0.25f;

        private static Vector2[] dist = new Vector2[2];
        public static float PathDistance(Vector3 a, Vector3 b)
        {
            dist[0].x = a.x;
            dist[0].y = a.z;

            dist[1].x = b.x;
            dist[1].y = b.z;

            return Mathf.Abs(Vector2.Distance(dist[0],dist[1]));
        }

        public static void ConvertPathIndex(NewWayPoint16 pt, Transform tr)
        {
            pt.x = (short)Mathf.Floor(tr.position.x);
            pt.z = (short)Mathf.Floor(tr.position.z);
        }

    }

    static public class GameBuildOption {

        static public float blockUnit = 3.0f;

        static private float min = 0.0f , max = 0.0f;
        static public float SnapBlockUnit(float pos)
        {
            min = Mathf.Floor(pos / GameBuildOption.blockUnit) * GameBuildOption.blockUnit;
            max = (Mathf.Floor(pos / GameBuildOption.blockUnit) + 1.0f) * GameBuildOption.blockUnit;
            ///float centerX = (min + max) / 2.0f;
            return (min + max) / 2.0f;

            //buildPoint.x = centerX;
        }




        // 확정된 방향을 검사한다.

        static NewWayPoint16 topPT    = new NewWayPoint16((short)(GameBuildOption.blockUnit * 0.0f), (short)(GameBuildOption.blockUnit * 1.0f));
        static NewWayPoint16 leftPT   = new NewWayPoint16((short)(GameBuildOption.blockUnit * -1.0f), (short)(GameBuildOption.blockUnit * 0.0f));
        static NewWayPoint16 rightPT  = new NewWayPoint16((short)(GameBuildOption.blockUnit * 1.0f), (short)(GameBuildOption.blockUnit * 0.0f));
        static NewWayPoint16 bottomPT = new NewWayPoint16((short)(GameBuildOption.blockUnit * 0.0f), (short)(GameBuildOption.blockUnit * -1.0f));
        static public void UpdateFourWayPoint(BuildDirOrder diroreder, Dictionary<int, NewWayPoint16> ways)//, DirIndex rotdir) // , Vector3 direction) 방향성을 가지는 벡터로 계산한다.
        {
            ways.Clear();

            byte dirCondi = (byte)diroreder;

            if ((dirCondi & DirIndexConst.top) == DirIndexConst.top)
            {
                ways.Add(DirIndexConst.top, topPT);
            }
            if ((dirCondi & DirIndexConst.left) == DirIndexConst.left)
            {
                ways.Add(DirIndexConst.left,leftPT);
            }
            if ((dirCondi & DirIndexConst.right) == DirIndexConst.right)
            {
                ways.Add(DirIndexConst.right,rightPT);
            }
            if ((dirCondi & DirIndexConst.bottom) == DirIndexConst.bottom)
            {
                ways.Add(DirIndexConst.bottom,bottomPT);
            }
        }
        static public void FourWayCheckPositions(BuildDirOrder diroreder,List<WayPoint16> ways)//, DirIndex rotdir) // , Vector3 direction) 방향성을 가지는 벡터로 계산한다.
        {
            byte dirCondi = (byte)diroreder;

            if ( (dirCondi & DirIndexConst.top) == DirIndexConst.top)
            {
                WayPoint16 dirpos = new WayPoint16();

                dirpos.x = (short)(GameBuildOption.blockUnit * 0.0f);
                dirpos.z = (short)(GameBuildOption.blockUnit * 1.0f);

                dirpos.AddWayDir(DirIndex.top);

                ways.Add(dirpos);
            }
            if ((dirCondi & DirIndexConst.left) == DirIndexConst.left)
            {
                WayPoint16 dirpos = new WayPoint16();

                dirpos.x = (short)(GameBuildOption.blockUnit * -1.0f);
                dirpos.z = (short)(GameBuildOption.blockUnit * 0.0f);

                dirpos.AddWayDir(DirIndex.left);

                ways.Add(dirpos);
            }
            if ((dirCondi & DirIndexConst.right) == DirIndexConst.right)
            {
                WayPoint16 dirpos = new WayPoint16();

                dirpos.x = (short)(GameBuildOption.blockUnit * 1.0f);
                dirpos.z = (short)(GameBuildOption.blockUnit * 0.0f);

                dirpos.AddWayDir(DirIndex.right);

                ways.Add(dirpos);
            }
            if ((dirCondi & DirIndexConst.bottom) == DirIndexConst.bottom)
            {
                WayPoint16 dirpos = new WayPoint16();

                dirpos.x = (short)(GameBuildOption.blockUnit * 0.0f);
                dirpos.z = (short)(GameBuildOption.blockUnit * -1.0f);

                dirpos.AddWayDir(DirIndex.bottom);

                ways.Add(dirpos);
            }
        }    
    }
    */
}

