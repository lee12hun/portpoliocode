﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace L.UIDefines
{
#if USENGUI
    public class NGUIFixed : DynamicStatic<NGUIFixed>
    {
        public bool IsNotSelectedUI()// ui 입력이 아니라면
        {
            return (UICamera.Raycast(Input.mousePosition) != true);

            // Mobile Touch 시 참고
            //return (Input.touchCount > 0 && UICamera.Raycast(Input.GetTouch(0).position) == false)
        }

        ///////////////////////////////////// Ngui Core Fixed        
                
        public void ChangeSprite(UISprite sprite, string name)
        {
            List<UISpriteData> lists = sprite.atlas.spriteList;

            for (int i = 0; i < lists.Count; i++)
            {
                if (lists[i].name.Contains(name))
                {
                    sprite.spriteName = lists[i].name;
                    break;
                }
            }
        }

        ///////////////////////////////////// Ngui Event 
        public void ClearOnClickEvent(UIButton btn)
        {
            btn.onClick.Clear();
        }

        public void AddOnClickEvent(System.Action call, UIButton btn)
        {
            if (null != call)
            {
                MonoBehaviour target = call.Target as MonoBehaviour;
                EventDelegate onClickEvent = new EventDelegate(target, call.Method.Name);

                btn.onClick.Clear();
                btn.onClick.Add(onClickEvent);

                //EventDelegate.Add(btn.onClick, onClickEvent);
            }
        }
    }
#endif
}


