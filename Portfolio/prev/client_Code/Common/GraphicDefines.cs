﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace L.GraphicDefines
{
    public class ShaderFixed : DynamicStatic<ShaderFixed>
    {
        //Material Shader Reset
        public void ReAssignShader(GameObject obj) // 분홍색처리되는 모델은 쉐이더를 다시 붙여준다.
        {
            Debug.Log("find object : " + obj);

            Renderer[] renderers = obj.transform.GetComponentsInChildren<Renderer>(true);

            foreach (Renderer item in renderers)
            {
                if (item.materials != null)
                {
                    foreach (Material mat in item.materials)
                    {
                        Shader sha = mat.shader;
                        sha = Shader.Find(sha.name);
                        // Debuger.Log(item.gameObject.name + " : " + mat.name, item.gameObject);
                    }
                }
            }
        }
    }

    public class Settings : DynamicStatic<Settings>
    {
        // Change SkyBox ----------------------------------------------------------
        public Material[] skyboxes;
        public void SetEnvironments(int env)
        {
            if (env < skyboxes.Length) RenderSettings.skybox = skyboxes[env];
        }
    }

    public class Images : DynamicStatic<Images>
    {
        // 로컬 위치에 이미지 로드 -------------------------------------------------------------------------
        public void CreateToLocalSprite(Image backgroundImg, string url)
        {
            Texture2D texture = null;
            if (System.IO.File.Exists(localpath))
            {
                byte[] byteTexture = System.IO.File.ReadAllBytes(localpath);

                if (byteTexture.Length > 0)
                {
                    texture = new Texture2D(0, 0);                    
                }
            }
            else
            {
                Debug.Log("File Exist Error " + localpath);
            }

            backgroundImg.sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
        }

        // Sprite to Texture2D ----------------------------------------------------------    
        public Texture2D textureFromSprite(Sprite sprite)
        {
            if (sprite.rect.width != sprite.texture.width)
            {
                Texture2D newText = new Texture2D((int)sprite.rect.width, (int)sprite.rect.height);

                newText.SetPixels(newColors);
                newText.Apply();
                return newText;
            }
            else
                return sprite.texture;
        }

        // WWW 이미지 로드 -------------------------------------------------------------------------
        private IEnumerator LoadBtnImageFromUrl(Image btnImage, string url)
        {
            //using (WWW www = new WWW(url))
            //yield return www;

            WWW www = new WWW(url);
            do
            {
                yield return null;
            } while (!www.isDone);

            www.Dispose(); // using

            if (!string.IsNullOrEmpty(www.error))
            {
                yield break; //LogManager.DebugLog(www.error, LogType.Error, "LoadImgUrl");
            }

            //yield return www;

            if (www.texture != null)
            {
                btnImage.enabled = true;
                Texture2D tex2d = www.texture;
                Rect rect = new Rect(0, 0, www.texture.width, www.texture.height);
                btnImage.color = Color.white;
                btnImage.type = Image.Type.Simple;
            }
        }
    }



}
