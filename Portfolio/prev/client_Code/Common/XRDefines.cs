﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace L.XRDefines
{ 
    public class Settings
    {
        public void CardBoardTextureHighPerformens()
        {
            // 기본 수치 0.7 
            // 좀더 선명하기 위해서는 1.0
            UnityEngine.XR.XRSettings.eyeTextureResolutionScale = 1.0f;
        }

        public void LoadDeviceByName() // 좀더 안정화 시킨 방법
        {
            // 초기 로드시 한번 불러들인다.
            if (UnityEngine.XR.XRSettings.loadedDeviceName.ToLower().Equals("cardboard") == false)
            {
                //string deviceName = vrEnabled ? "Cardboard" : "";
                UnityEngine.XR.XRSettings.LoadDeviceByName("Cardboard");
            }

            // 이후 Cardboard 모드를 켜고 끄는 방법
            UnityEngine.XR.XRSettings.enabled = true;
            UnityEngine.XR.XRSettings.enabled = false;
        }
    }
}
