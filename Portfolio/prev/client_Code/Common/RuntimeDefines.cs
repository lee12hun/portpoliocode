﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace L.RuntimeDefines
{
    public class FilePath : DynamicStatic<FilePath>
    {
        public readonly string LocalFilePath = Application.persistentDataPath;

        // 해당 경로에 폴더가 없으면 생성
        public string DirectoryPath(string fileroot = "/store/files")
        {
            string directory = Application.persistentDataPath + fileroot;

            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            return directory + "/";
        }

        public void WriteLogFiles(String message, string filename)
        {
            string filepath = DirectoryPath() + filename;

            StreamWriter logStream = File.CreateText(filepath);

            //StreamWriter logStream = null;
            //logStream = new StreamWriter(directory + filename);//, false, System.Text.Encoding.UTF8);
            //logStream.AutoFlush = true;
            logStream.Write(message);
            logStream.Close();
        }

        // 저장된 파일 경로 삭제
        public void Delete(string sourcePath)
        {
            string paths = Application.persistentDataPath + "/" + sourcePath;

            if (System.IO.File.Exists(paths))
            {
                try
                {
                    System.IO.File.Delete(paths);
                }
                catch (System.IO.IOException e)
                {
                    System.Console.WriteLine(e.Message);
                    return;
                }
            }
        }
    }

    public class FilePathRuntime : DynamicStatic<FilePathRuntime>
    {
        public  String ResourceRootPath = "Prototype001_01/";
        //public  String SaveDataPath = "SaveDatas/";
        public  String SaveJsonPath = "SaveDatas/Json/";
        public  String SaveSystemPath = "SaveDatas/System/";

        public List<String> TablePath = null;//"Tables/";

        private string[] SupportExtensions = new string[] { ".txt", ".html", ".htm", ".xml", ".bytes", ".json", ".csv", ".yaml", ".fnt" };

        public enum SearchType
        {
            Json,
            System,
            Table
        }
        
        public String GetAssetPath(String targetname, SearchType type)
        {
            //delete extendtion 
            for (int i = 0; i < SupportExtensions.Length; i++)
            {
                if (targetname.EndsWith(SupportExtensions[i]))
                {
                    targetname = targetname.Remove(targetname.Length - SupportExtensions[i].Length);
                    break;
                }
            }


            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            TextAsset result = null;            

            if(result)
            {
                return sb.ToString();
            }
            else
            {
                return null;
            }
        }
    }

    public class SystemEnvironment : DynamicStatic<SystemEnvironment>
    {
        public bool IsNetworkConnected()
        {
            bool isAbailableInternet = false;
            switch (Application.internetReachability)
            {
                case NetworkReachability.ReachableViaLocalAreaNetwork:   // Wifi
                    isAbailableInternet = true;
                    break;
                case NetworkReachability.ReachableViaCarrierDataNetwork: // 3G
                    isAbailableInternet = true;
                    break;
                case NetworkReachability.NotReachable:
                default:
                    isAbailableInternet = false;
                    break;
            }

            return isAbailableInternet;
        }


        public void FixedFrameRate()
        {
            Application.targetFrameRate = 60;
        }

        

        
    }
}
