﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.U2D;


public class BundlePatchSetting
{
    public delegate void PathProgress(string filename, int current, int total);

    bool isLocal;
    public string platformName;
    public string ftpServerURL;

    public PathProgress processPatch = null;
    public System.Action completePatch = null;

    public bool IsLocal { get { return isLocal; } }

    public BundlePatchSetting(bool islocal = false)
    {
        isLocal = islocal;
        DefaultSetting();
    }    

    void DefaultSetting()
    {
        platformName = "StandaloneWindows";

        if (isLocal)
        {
            ftpServerURL = "./_BundleManaging/_FileServer/" + platformName + "/";
        }
        else
        {
            ftpServerURL = "ftp://127.0.0.1:21/" + platformName + "/";
        }

        Debug.Log(ftpServerURL);
    }
}

public class BundlePatch : MonoBehaviour
{
    private BundlePatch() { }

    class BundleInfo
    {
        public string bundleName { get; private set; }
        public Hash128 hash128 { get; private set; }

        public BundleInfo(string bundlename, Hash128 hash128)
        {
            this.bundleName = bundlename;
            this.hash128 = hash128;
        }
    }

    class PrefabIndex
    {
        public int bundleID;
        public int prefabID;

        public PrefabIndex(int b, int p)
        {
            bundleID = b;
            prefabID = p;
        }
    }

    class BundleNameIndexer
    {
        int createIndex = 10000;
        Dictionary<int, string> pathIndex = new Dictionary<int, string>();

        public int AddName(string path)
        {
            var etor = pathIndex.GetEnumerator();
            while (etor.MoveNext())
            {
                if (etor.Current.Value.Equals(path))
                {
                    Debug.LogError("Duplication Value");
                    return -1;
                }
            }

            int result = createIndex;
            pathIndex.Add(createIndex, path);
            createIndex++;

            return result;
        }
        public string GetName(int index)
        {
            return pathIndex[index];
        }
    }

    string serverBundleManifestFile;

    string localBundleFolder;
    string localBundleManifestFile;

    BundlePatchSetting setting = null;

    BundleNameIndexer nameIndexer = new BundleNameIndexer();
    Dictionary<string, PrefabIndex> prefabIndex = new Dictionary<string, PrefabIndex>();
    Dictionary<string, GameObject> instGameObjects = new Dictionary<string, GameObject>();

    public static BundlePatch Create(Transform parent, BundlePatchSetting set)
    {
        GameObject obj = new GameObject("bundlepatch");

        obj.transform.parent = parent;

        BundlePatch component = obj.AddComponent<BundlePatch>();
        component.CreateSetting(set);

        return component;
    }

    void Awake()
    {

    }

    void CreateSetting(BundlePatchSetting set)
    {
        setting = set;

        serverBundleManifestFile = setting.ftpServerURL + setting.platformName;

        localBundleFolder = Application.persistentDataPath + "/AssetBundles/";

        localBundleManifestFile = localBundleFolder + setting.platformName;

    }

    public void StartPatch()
    {
        StartCoroutine(BundleAutoPatch());
    }

    public bool IsFirstUser()
    {
        bool res = false;

        if (!Directory.Exists(localBundleFolder))
        {
            Directory.CreateDirectory(localBundleFolder);

            res = true;
        }

        if (false == System.IO.File.Exists(localBundleManifestFile))
        {
            res = true;
        }

        return res;
    }    

    IEnumerator DownloadManifestFile(List<BundleInfo> serverbundles)
    {
        AssetBundleManifest manifest = null;
        UnityWebRequest webrequest = null;
        AssetBundle assetBundle = null;

        if (setting.IsLocal)
        {
            byte[] bytes = File.ReadAllBytes(serverBundleManifestFile);
            File.WriteAllBytes(localBundleManifestFile, bytes); // local file save
            Debug.Log("<color=green>" + "manifest file SAVE : " + localBundleManifestFile + "</color>");

            AssetBundleCreateRequest req = AssetBundle.LoadFromFileAsync(serverBundleManifestFile);
            yield return req;
            assetBundle = req.assetBundle;
        }        

        manifest = assetBundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");

        foreach (var bundlename in manifest.GetAllAssetBundles())
        {
            serverbundles.Add(new BundleInfo(bundlename, manifest.GetAssetBundleHash(bundlename)));
        }
        assetBundle.Unload(false);

        if (null != webrequest)
        {
            webrequest.Dispose();
        }
    }

    IEnumerator BundleDownloadAndSave(string url, string fileName)
    {
        string savefilename = localBundleFolder + fileName;

        if (setting.IsLocal)
        {
            byte[] bytes = File.ReadAllBytes(url);

            File.WriteAllBytes(savefilename, bytes);
            yield return null;
        }

        Debug.Log("<color=green>" + "bundle file SAVE : " + savefilename + "</color>");
    }

    IEnumerator LocalBundleNameRegist()
    {
        AssetBundleCreateRequest req = AssetBundle.LoadFromFileAsync(localBundleManifestFile);
        yield return req;

        AssetBundle manifestbundle = req.assetBundle;
        if (manifestbundle == null)
        {
            yield break;
        }

        AssetBundleManifest manifest = manifestbundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");

        foreach (var bundlename in manifest.GetAllAssetBundles()) // toLower
        {
            int bundleid = nameIndexer.AddName(bundlename);

            AssetBundleCreateRequest bundle = AssetBundle.LoadFromFileAsync(localBundleFolder + bundlename);
            yield return bundle;

            AssetBundle assetBundle = bundle.assetBundle;

            string[] names = assetBundle.GetAllAssetNames(); // toLower

            for (int i = 0; i < names.Length; i++)
            {
                string prefabname = FrontName(BackName(names[i], "/"),".");

                int prefabid = nameIndexer.AddName(names[i]);

                prefabIndex.Add(prefabname.ToLower(),new PrefabIndex(bundleid, prefabid));
            }
            assetBundle.Unload(true);
        }

        manifestbundle.Unload(true);
    }
    
    public void Clear()
    {
        Resources.UnloadUnusedAssets();
    }
}
