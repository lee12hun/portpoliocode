﻿using JsonClasses;
using System.Collections;
using System.Collections.Generic;
using System.Numerics.Extensions;
using UnityEngine;

// Pause , Quit 기능 활성화 

public class DataStorage : SingleMono<DataStorage>
{
    protected JsonUtilRegistry<JsStorage> datas;
    protected JsonStorageFinder finder = null;

    protected TableManager tableManager = null;
    protected TableDataFinder tableDataFinder = null;

    const string referencefileName = "data.json";

#if UNITY_EDITOR
    const string fileName = "data.dat";
#else
    const string fileName = "data.dat";
#endif

    public void Awake()
    {
        instance = this;

        // JsonFile Load
        datas = new JsonUtilRegistry<JsStorage>(fileName, "Data/" + referencefileName, false);
        finder = new JsonStorageFinder();

        // Table Laod
        tableManager = new TableManager();
        tableDataFinder = new TableDataFinder();

        DontDestroyOnLoad(instance.gameObject);
    }


    public static JsStorage Datas
    {
        get
        {
            return Instance.datas.jsData;
        }
    }

    public static TableManager Table
    {
        get
        {
            return Instance.tableManager;
        }
    }
}