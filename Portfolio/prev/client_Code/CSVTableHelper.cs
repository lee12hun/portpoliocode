﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CSVReader
{
    public class LBaseTable{}

    public class CSVBaseTable{};

    public class CSVTable<T> : CSVBaseTable where T : LBaseTable, new() 
    {
        public enum TableIDType
        {
            String,
            Integer
        };

        private Dictionary<int, T> indexCacheRecords = new Dictionary<int, T>();

        //private Dictionary<obejct, T> cacheRecords = new Dictionary<obejct, T>();
        private Dictionary<string, int> stringRecords = null;// new Dictionary<object, T>();
        private Dictionary<int, int> integerRecords = null;// new Dictionary<object, T>();

        private TableIDType defaultIDType;

        public CSVTable(string file, TableIDType type = TableIDType.Integer)
        {
            defaultIDType = type;

            CSVTable table = CSVTable.Load(file);

            if (TableIDType.String == defaultIDType)
            {
                CreateStringKeyTable(file, table);
            }
            else if (TableIDType.Integer == defaultIDType)
            {
                CreateIntegerKeyTable(file, table);
            }
        }

        private void CreateStringKeyTable(string file, CSVTable table)
        {
            stringRecords = new Dictionary<string, int>();

            for (int i = 0; i < table.Count(); i++)
            {
                table.SetDefaultIndex(i);

                string tableid = null;

                tableid = table[table.fieldNames[0][0]];
#if UNITY_EDITOR
                try
                {
#endif
                    T data = table.ConvertClass<T>();
                    indexCacheRecords.Add(i, data);
                    stringRecords.Add(tableid, i);                    
#if UNITY_EDITOR
                }
                catch (Exception e)
                {
                    FLogManager.DebugLogError("Table Load Error] TableName : " + file + " , tableid : " + tableid);
                    throw e;
                }
#endif
            }
        }
        private void CreateIntegerKeyTable(string file, CSVTable table)
        {
            integerRecords = new Dictionary<int, int>();

            for (int i = 0; i < table.Count(); i++)
            {
                table.SetDefaultIndex(i);

                int tableid = 0;
                tableid = int.Parse(table[table.fieldNames[0][0]]);
#if UNITY_EDITOR
                try
                {
#endif
                    T data = table.ConvertClass<T>();
                    indexCacheRecords.Add(i, data);
                    integerRecords.Add(tableid, i);
                    
#if UNITY_EDITOR
                }
                catch (Exception e)
                {
                    FLogManager.DebugLogError("Table Load Error] TableName : " + file + " , tableid : " + tableid);
                    throw e;
                }
#endif
            }
        }

        //private bool IsValid(object o)
        //{
        //    if (TableIDType.Integer == defaultIDType)
        //    {
        //        return (o is int);
        //    }
        //    else if (TableIDType.String == defaultIDType)
        //    {
        //        return (o is string);
        //    }
        //    return false;
        //}

        //public T ID(object rowId)
        public T ID(string rowId)
        {
            //if (IsValid(rowId))
            if (TableIDType.Integer == defaultIDType)
            {
                FLogManager.DebugLog("TableId Type Error : use to " + defaultIDType);
            }
            else if (stringRecords.ContainsKey(rowId))
            {
                return Index(stringRecords[rowId]);
            }

            return null;
        }
        public T ID(int rowId)
        {
            //if (IsValid(rowId))
            if (TableIDType.String == defaultIDType)
            {
                FLogManager.DebugLog("TableId Type Error : use to " + defaultIDType);
            }
            else if (integerRecords.ContainsKey(rowId))
            {
                return Index(integerRecords[rowId]);
            }

            return null;
        }

        public int Count
        {
            get
            {
                return indexCacheRecords.Count;
            }
        }
        public T Index(int index)
        {
            return indexCacheRecords[index];
        }
    }

    // Cache Table, 
    public class CSVTableHelper
    {
        protected static string tableFolder = "Table/";//"integrationtable/";

        private Dictionary<System.Type, CSVBaseTable> cacheTables = new Dictionary<System.Type, CSVBaseTable>();

        public CSVTableHelper() { }
        public CSVTableHelper(string foldername){ tableFolder = foldername; }

        public void AddTable<T>() where T : LBaseTable, new()
        {
            System.Type t = typeof(T);
            string filename = t.Name;

            if (false == cacheTables.ContainsKey(t))
            {
                CSVTable<T> table = new CSVTable<T>(tableFolder + filename);
                cacheTables.Add(t, table);
                //FLogManager.DebugLog("[ADD Table] : " + t + " filename : " + filename);
            }
        }

        public CSVTable<T> GetTable<T>(string filename = "") where T : LBaseTable, new()
        {
            System.Type t = typeof(T);

            if (false == cacheTables.ContainsKey(t))
            {
                if (string.IsNullOrEmpty(filename))
                {
                    filename = t.Name;
                }
                
                AddTable<T>();
            }   

            return cacheTables[t] as CSVTable<T>;
        }

        public void DeleteTable<T>() where T : LBaseTable//, new()
        {
            System.Type t = typeof(T);

            if (cacheTables.ContainsKey(t))
            {
                cacheTables[t] = null;

                Resources.UnloadUnusedAssets();
                System.GC.Collect();
            }
        }

        public T GetRecord<T>(string index) where T : LBaseTable, new()
        {
            CSVTable<T> table = GetTable<T>();
            return table.ID(index);
        }
        public T GetRecord<T>(int index) where T : LBaseTable, new()
        {
            CSVTable<T> table = GetTable<T>();
            return table.ID(index);
        }
    }
}

