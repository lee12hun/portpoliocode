﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using L.FileDatas;

public interface IBuildEvent
{
}
public interface IBuildLoadEvent : IBuildEvent
{
    void OnBuildLoad(JFxData data);
}
public interface IBuildPressEvent : IBuildEvent
{
    void OnBuildPress();
}
public interface IBuildCompleteEvent : IBuildEvent
{
    void OnBuildComplete();
}
public interface IBuildDeleteEvent : IBuildEvent
{
    void OnBuildDelete();
}
