﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using L.FileDatas;


public class UserDataBuildLoadProcedural : MonoBehaviour {

	
	void Start () {
       

        JFxData.LoopDic(GameDataStorage.Instance.buildData.Dic("BuildComplete"), (root) =>
        {
            JFxData element = new JFxData(root);

            Transform obj = InstObjectsPools.Instance.Spawn(element.Str("prop"));
            
            Debug.Log(element.RootKey());

            if (obj)
            {
                obj.GetComponent<IBuildLoadEvent>().OnBuildLoad(element);
            }
        });

    }
}
