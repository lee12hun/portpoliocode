﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbLoadProcess : MonoBehaviour , IBlockLoadProcess
{
    // delete ---------------------------
    public IEnumerator CreateProcess()
    {
        gameObject.SetActive(true);

        Debug.Log(" AbLoadProcess : " + gameObject.name);


        yield return StartCoroutine(Create());
    }
    //public abstract IEnumerator Create();
    public virtual IEnumerator Create() { yield return null; }
    // delete ---------------------------



    public virtual void StartLoading()
    {
        gameObject.SetActive(true);
    }

    public virtual float GetLoadingRate()
    {
        return 0f;
    }
    public virtual bool IsLoadComplete()
    {
        return false;
    }
}
