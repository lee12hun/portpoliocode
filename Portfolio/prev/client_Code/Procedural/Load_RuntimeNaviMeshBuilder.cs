﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Test Class
namespace TempCode
{
    public class LocalNavMeshBuilder
    {
        public IEnumerator AsyncUpdateNavMesh() { yield return null; }
    }
}

public class Load_RuntimeNaviMeshBuilder : AbLoadProcess
{
    TempCode.LocalNavMeshBuilder builder = null;

    public override IEnumerator Create()
    {
        builder = GetComponent<TempCode.LocalNavMeshBuilder>();

        yield return StartCoroutine(builder.AsyncUpdateNavMesh());

        yield return null;
    }
}
