﻿using JsonClasses;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BaseScrollViewGUIManager<T> : MonoBehaviour where T : BaseScrollViewGUIItem
{
    public T itemPrefab;

    public LayoutGroup contentRoot;

    public List<T> items = new List<T>();

    public virtual void Awake()
    {
        itemPrefab.Active = false;

        items.Add(itemPrefab);
    }

    public virtual void Clear()
    {
        for (int i = 0; i < items.Count; i++)
        {
            items[i].Active = false;
        }
    }

    private T MakeActiveItem()
    {
        T activeitem = null;

        for (int i = 0; i < items.Count; i++)
        {
            if (false == items[i].Active)
            {
                activeitem = items[i];
                break;
            }
        }

        if (activeitem == null)
        {
            T item = GameObject.Instantiate<T>(itemPrefab, contentRoot.transform);
            items.Add(item);
            activeitem = item;
        }

        //activeitem.transform.parent = contentRoot.transform;

        activeitem.Active = true;

        return activeitem;
    }
    public int Count
    {
        get
        {
            return items.Count;
        }
    }

    public virtual T AddItem(int dataid)
    {
        T activeitem = MakeActiveItem();

        activeitem.SetData(dataid);
        activeitem.UIRefresh();

        return activeitem;
    }

    public T FindID(int dataid)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].IsEqualID(dataid))
            {
                return items[i];
            }
        }

        return null;
    }



}
