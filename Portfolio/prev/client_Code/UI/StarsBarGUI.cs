﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class StarsBarGUI : MonoBehaviour
{
    //[Header("Resources")]
    public HorizontalLayoutGroup group;
    public List<Image> stars;

#if UNITY_EDITOR
    [Header("EditorOnly")]
    public bool isNotAligment;
    public void OnValidate()
    {
        if (isNotAligment)
        {
            GetComponent<HorizontalLayoutGroup>().enabled = false;
            GetComponent<ContentSizeFitter>().enabled = false;
        }
        else
        {
            GetComponent<HorizontalLayoutGroup>().enabled = true;
            GetComponent<ContentSizeFitter>().enabled = true;
        }
    }
#endif

    public void ActvieStarImages(int level)
    {
        //level += ConstValue.TempInitLevelData;

        if (level > 0)
        {
            for (int i = 0; i < stars.Count; i++)
            {
                if (i < level)
                {
                    stars[i].gameObject.SetActive(true);
                }
                else
                {
                    stars[i].gameObject.SetActive(false);
                }
            }
        }
        else
        {
            Debug.LogError("Error] ActvieStar ,  level 0");
        }
    }
}