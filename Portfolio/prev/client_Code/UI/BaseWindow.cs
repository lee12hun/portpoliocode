﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseWindow : MonoBehaviour
{
    public enum OpenType // editer에서 정한 기본값
    {
        Single,
        Overlap
    }

    public enum CloseType
    {
        Once,//Upgrade , Result // 조건 만족시 나타났다 사라지는 윈도우 
        Cache,//Info, // 기본 상시 로드
    }

    public OpenType openType;
    public CloseType closeType;
    public virtual void Active(bool active)
    {
        gameObject.SetActive(active);
    }
    public virtual void SetData(object data) { }
    public abstract void UIRefresh();
}
