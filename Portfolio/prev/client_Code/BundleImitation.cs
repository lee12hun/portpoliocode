﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class BundleImitation : MonoBehaviour
{
    protected RegistPrefabRoot registPrefabs;

    public static BundleImitation Create(Transform parent, RegistPrefabRoot prefab)
    {
        GameObject obj = new GameObject("ImitateBundle");

        obj.transform.parent = parent;

        BundleImitation component = obj.AddComponent<BundleImitation>();

        component.registPrefabs = prefab;
        //component.Load();

        return component;
    }

    public Transform FindPrefabs(string prefabname)
    {
        Transform res = null;

        for (int i = 0; i < registPrefabs.prefabs.Count; i++)
        {
            if (registPrefabs.prefabs[i].name.Contains(prefabname))
            {
                res = registPrefabs.prefabs[i].transform;
                return res;
            }

            res = registPrefabs.prefabs[i].Find(prefabname);

            if (res)
            {
                return res;
            }
        }

        FLogManager.DebugLogError("InstObjectsPools Error] Find not Prefabs : " + prefabname);

        return null;
    }

    public SpriteAtlas FindAtlas(string atlasname)
    {
        SpriteAtlas res = null;

        if (null == res)
        {
            for (int i = 0; i < registPrefabs.atlas.Count; i++)
            {
                res = registPrefabs.atlas[i].Find(atlasname);

                if (res)
                {
                    break;
                }   
            }
        }

        if (null == res)
            FLogManager.DebugLogError("FindAtlas Error] atlas name : " + atlasname);

        return res;
    }
}
