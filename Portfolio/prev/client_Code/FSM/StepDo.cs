﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StepDo : MonoBehaviour
{
    public abstract void Act(FlowControl controller);
}
