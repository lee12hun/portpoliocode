﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Step : MonoBehaviour {

    public StepDo[] actions;

    public StepTransit[] transitions;

    public StepDone[] doneActions;

    public void ChangeStep(FlowControl controller)
    {
        for (int i = 0; i < actions.Length; i++)
        {
            if (actions[i].gameObject.activeSelf == true)
            {
                actions[i].Act(controller);
                Debug.Log("Change Step " + transform.name + " Action " + actions[i].name);
            }
        }
    }

    public void UpdateStep(FlowControl controller)
    {
        //DoAction(controller);

        CheckTransistion(controller);
    }

    public void EndStep(FlowControl controller)
    {
        for (int i = 0; i < doneActions.Length; i++)
        {
            if (doneActions[i].gameObject.activeSelf == true)
            {
                doneActions[i].Act(controller);
                Debug.Log("End Step " + transform.name + " Action " + doneActions[i].name);
            }
                
        }
    }

    private void CheckTransistion(FlowControl controller)
    {
        for (int i = 0; i < transitions.Length; i++)
        {
            bool decisionSucceeded = transitions[i].decision.Decide(controller);

            if(transitions[i].nextstep > 0)
            {
                decisionSucceeded = true;
                transitions[i].nextstep = 0;
            }

            if (decisionSucceeded)
            {
                controller.TransitionToState(transitions[i].trueState);

                //Debug.Log("End Step " + transform.name + " Action " + actions[i].name);
            }
            else
            {
                controller.TransitionToState(transitions[i].falseState);
            }
        }
    }
}
