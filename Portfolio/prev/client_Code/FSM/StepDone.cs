﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StepDone : MonoBehaviour
{
    public abstract void Act(FlowControl controller);
}
