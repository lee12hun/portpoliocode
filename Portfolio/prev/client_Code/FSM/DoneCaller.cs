﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DoneCaller : StepDone
{
    public UnityEvent call;

    public override void Act(FlowControl controller)
    {
        call.Invoke();
    }
}
