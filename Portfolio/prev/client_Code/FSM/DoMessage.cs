﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoMessage : StepDo
{
    //public float activeTime = 1;

    public float DealyTime =0f;

    public PopupMessageManager.MessageType type;
    public string text;
    public string title="";
    public float time = 0f;
    public Sprite imagenames;

    public override void Act(FlowControl controller)
    {
        StartCoroutine(ActiveMessageTime());
    }

    IEnumerator ActiveMessageTime()
    {
        yield return new WaitForSeconds(DealyTime);

        if(PopupMessageManager.MessageType.ImagePopup == type)
        {
            PopupMessageManager.Instance.OpenImagePopup(imagenames, time);
        }
        else
        {
            PopupMessageManager.Instance.OpenPopup(type, text, title, time);
        }        
    }
}
