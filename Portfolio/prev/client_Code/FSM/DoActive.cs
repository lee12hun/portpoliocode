﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoActive : StepDo
{
    public bool isActive;

    public List<Collider> coll;
    public List<Canvas> canvas;
    public List<Transform> lists;

    public override void Act(FlowControl controller)
    {
        for (int i = 0; i < coll.Count; i++)
        {
            coll[i].enabled = isActive;

            if(coll[i].GetComponent<Renderer>())
            {
                coll[i].GetComponent<Renderer>().enabled = isActive;
            }

        }

        for (int i = 0; i < canvas.Count; i++)
        {
            canvas[i].gameObject.SetActive(isActive);

            if (canvas[i].name.Contains("CanvasESDMsg"))
            {
                Debug.Log(" name " + transform.parent.name);
            }
        }

        for (int i = 0; i < lists.Count; i++)
        {
            lists[i].gameObject.SetActive(isActive);

            if (lists[i].name.Contains("CanvasESDMsg"))
            {
                Debug.Log(" name " + transform.parent.name);
            }
        }
    }
}

