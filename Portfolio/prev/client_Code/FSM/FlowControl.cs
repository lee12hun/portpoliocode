﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowControl : MonoBehaviour {

    public Step current;
    public Step remain;

    public PlayerStats stats;

    public void Start()
    {
        current.ChangeStep(this);
    }

    private void Update()
    {
        current.UpdateStep(this);
    }

    public void TransitionToState(Step next)
    {
        if(next.name.ToLower().Contains("remain"))
        {
            return;
        }

        if (next != remain)
        {
            current.EndStep(this);
            next.ChangeStep(this);

            current = next;

            OnExitState();            
        }
    }

    private void OnExitState()
    {
        //stateTimeElapsed = 0;
    }
}
