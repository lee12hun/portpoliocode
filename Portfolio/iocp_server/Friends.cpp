#include "MainServerPCH.h"

CFriends* CFriends::m_CFriends = NULL;

CFriends::CFriends()
{
	prepares.Constructor(PrepareParams::COUNT);
}
// Init , Release
void CFriends::Initialize()
{

}

void CFriends::Release()
{
	prepares.Release();
}

void CFriends::InitCBFunction()
{
	g_pIOCPEngine->SetPacketClientCB(PK_FRIEND_FIND_LIST, 0, ProcFriendFindList);
	g_pIOCPEngine->SetPacketClientCB(PK_FRIEND_REQUEST, 0, ProcFriendRequest);
	g_pIOCPEngine->SetPacketClientCB(PK_FRIEND_REQUEST_LIST, 0, ProcFriendRequestList);
	g_pIOCPEngine->SetPacketClientCB(PK_FRIEND_REQUEST_CANCEL, 0, ProcFriendRequestCancel);
	g_pIOCPEngine->SetPacketClientCB(PK_FRIEND_RECEIVED_LIST, 0, ProcFriendReceivedList);
	g_pIOCPEngine->SetPacketClientCB(PK_FRIEND_RECEIVED_CONFIRM, 0, ProcFriendReceivedConfirm);
	g_pIOCPEngine->SetPacketClientCB(PK_FRIEND_REGISTED_LIST, 0, ProcFriendRegistedList);
	g_pIOCPEngine->SetPacketClientCB(PK_FRIEND_REGISTED_MEMO, 0, ProcFriendRegistedMemo);
	g_pIOCPEngine->SetPacketClientCB(PK_FRIEND_BLOCK, 0, ProcFriendBlock);
	g_pIOCPEngine->SetPacketClientCB(PK_FRIEND_BLOCK_LIST, 0, ProcFriendBlockList);
	g_pIOCPEngine->SetPacketClientCB(PK_FRIEND_REQUEST_DELETE, 0, ProcFriendRequestDelete);
	g_pIOCPEngine->SetPacketClientCB(PK_FRIEND_UNBLOCK, 0, ProcFriendUnblock);
	g_pIOCPEngine->SetPacketClientCB(PK_FRIEND_DELETE, 0, ProcFriendDelete);
	g_pIOCPEngine->SetPacketClientCB(PK_FRIEND_REQUEST_BRING, 0, ProcFriendRequestBring);
	g_pIOCPEngine->SetPacketClientCB(PK_FRIEND_REQUEST_BRING_AGREE, 0, ProcFriendRequestBringAgree);
	g_pIOCPEngine->SetPacketClientCB(PK_FRIEND_REQUEST_CHASE, 0, ProcFriendRequestChase);

}

void CFriends::InitDBPrepare(THMySqlConnector* pConn)
{
	DBTryCatch::TryPrepare(DBTraceGetParam(this, &pConn)
	{
		sql::Connection* pSqlConn = pConn->m_pConn;
		//
		prepares[selectFindAllCharacterList] = pSqlConn->prepareStatement("SELECT CHGUID, nickName, TID, Profil_style_ID,  Profil_ID FROM characters WHERE ACGUID NOT IN(?) ORDER BY RAND() LIMIT 20");		
		prepares[selectFindCharacterList] = pSqlConn->prepareStatement("SELECT CHGUID, nickName, TID, Profil_style_ID,  Profil_ID FROM characters WHERE ACGUID NOT IN(?)	AND nickName LIKE(?) LIMIT 20");

		//
		prepares[insertFriendRequest] = pSqlConn->prepareStatement("INSERT INTO friends_request(CHGUID,requestor_CHGUID,nickname,requestor_nickname) VALUES(?,?,?,?)");
		prepares[selectFriendRequest_RecvList] = pSqlConn->prepareStatement("SELECT requestor_CHGUID, requestor_nickname FROM friends_request WHERE CHGUID IN(?) LIMIT 100");
		prepares[selectFriendRequest_SendList] = pSqlConn->prepareStatement("SELECT CHGUID, nickname FROM friends_request WHERE requestor_CHGUID IN(?) LIMIT 100");
		prepares[selectFriendRequest_FindID] = pSqlConn->prepareStatement("SELECT CHGUID, requestor_CHGUID FROM friends_request WHERE CHGUID=? AND requestor_CHGUID=?");
		prepares[deleteFriendRequest] = pSqlConn->prepareStatement("DELETE FROM friends_request WHERE CHGUID=? AND requestor_CHGUID=?");

		//
		prepares[insertFriend] = pSqlConn->prepareStatement("INSERT INTO friends_regist(CHGUID,friend_CHGUID,nickname,friend_nickname,friend_state) VALUES(?,?,?,?,1)");
		
		prepares[selectFriendList_CHGUID]		  = pSqlConn->prepareStatement("SELECT friend_CHGUID, friend_nickname, memo, friendship_count, bestfriend_state FROM friends_regist WHERE CHGUID=? AND front_block=0 AND back_block=0 LIMIT 100");
		prepares[selectFriendList_friend_CHGUID] = pSqlConn->prepareStatement("SELECT CHGUID, nickname, friend_memo, friendship_count, bestfriend_state FROM friends_regist WHERE friend_CHGUID=? AND front_block=0 AND back_block=0 LIMIT 100");
		prepares[selectFriendRegist_FindID] = pSqlConn->prepareStatement("SELECT CHGUID, friend_CHGUID FROM friends_regist WHERE CHGUID=? AND friend_CHGUID=? AND front_block=0 AND back_block=0");

		prepares[updateFriendMemo_memo] = pSqlConn->prepareStatement("UPDATE friends_regist SET memo=? WHERE CHGUID=? AND friend_CHGUID=?");
		prepares[updateFriendMemo_friend_memo] = pSqlConn->prepareStatement("UPDATE friends_regist SET friend_memo=? WHERE CHGUID=? AND friend_CHGUID=?");		
		prepares[updateFriendCount] = pSqlConn->prepareStatement("UPDATE characters SET friends_count=? WHERE CHGUID=?");

		//
		prepares[selectFriendList_Block] = pSqlConn->prepareStatement("SELECT CHGUID, friend_CHGUID, nickname, friend_nickname, memo, friend_memo, front_block, back_block, friend_state, bestfriend_state, friendship_count FROM friends_regist WHERE (CHGUID=? AND back_block=1) OR (friend_CHGUID=? AND front_block=1)");
		prepares[selectFriendList_Block_FindID] = pSqlConn->prepareStatement("SELECT CHGUID, friend_CHGUID, nickname, friend_nickname, memo, friend_memo, front_block, back_block, friend_state, bestfriend_state, friendship_count FROM friends_regist WHERE (CHGUID=? AND friend_CHGUID=?) OR (CHGUID=? AND friend_CHGUID=?)");
		prepares[insertFriendBlock_CHGUID] = pSqlConn->prepareStatement("INSERT INTO friends_regist(CHGUID, friend_CHGUID, nickname, friend_nickname, back_block) VALUES(?,?,?,?,1)");		
		
		prepares[updateFriendBlock] = pSqlConn->prepareStatement("UPDATE friends_regist SET front_block=?, back_block=?, friend_state=?, bestfriend_state=? WHERE CHGUID=? AND friend_CHGUID=?");		
		prepares[deleteFriendBlock] = pSqlConn->prepareStatement("SELECT friends_unblock(? , ?)");
		prepares[deletefriend] = pSqlConn->prepareStatement("SELECT friends_delete(? , ?)");

		// select friends , block count 
		prepares[selectCharacterFriendCount] = pSqlConn->prepareStatement("SELECT friends_count, block_character_count FROM characters WHERE CHGUID=?");
		prepares[selectFriendList_addcloumn] = pSqlConn->prepareStatement("SELECT TID, Profil_style_ID, Profil_ID, Logout_Time FROM characters WHERE CHGUID=?");
		prepares[update_friend_request_confirm] = pSqlConn->prepareStatement("SELECT friends_request_confirm(? , ?, ?, ?)");
		prepares[update_friend_request_cancel] = pSqlConn->prepareStatement("SELECT friends_request_cancel(? , ?)");
		prepares[select_friend_request_info] = pSqlConn->prepareStatement("SELECT CASE WHEN friends_regist.CHGUID = ? and friends_regist.friend_CHGUID = ? THEN friends_regist.memo WHEN friends_regist.CHGUID = ? and friends_regist.friend_CHGUID = ? THEN friends_regist.friend_memo END as memo, friendship_count, bestfriend_state, characters.TID, characters.Profil_style_ID, characters.Profil_ID, characters.Logout_Time FROM friends_regist INNER JOIN characters ON characters.CHGUID = ?	WHERE(friends_regist.CHGUID = ? and friends_regist.friend_CHGUID = ?) OR(friends_regist.CHGUID = ? and friends_regist.friend_CHGUID = ?) ");
		prepares[select_friend_list] = pSqlConn->prepareStatement("SELECT CASE WHEN friends_regist.CHGUID = ? and friends_regist.friend_state = 1 THEN friend_CHGUID WHEN friends_regist.friend_CHGUID = ? and friends_regist.friend_state = 1 THEN friends_regist.CHGUID END as Friends_CHGUDI from friends_regist where friends_regist.CHGUID = ? or friends_regist.friend_CHGUID = ?");
		prepares[update_friend_LogOut_Time] = pSqlConn->prepareStatement("UPDATE characters SET Logout_Time=? where CHGUID = ?");
		prepares[check_friend] = pSqlConn->prepareStatement("select count(*) from nextor_meta.friends_regist where friend_state = 1 and ((CHGUID = ? and friend_CHGUID = ?) or (CHGUID = ? and friend_CHGUID = ?))");
	});

}

void CFriends::ReleaseDBCommands()
{
	prepares.Release();
}

// PK_FRIEND_FIND_LIST 
BOOL CFriends::ProcFriendFindList(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen)
{
	//-----------------------------------------------------------------------
	// Base Setting

	_PACKET_ID_CLIENT packetID = PK_FRIEND_FIND_LIST;

	PX_BUFFER* pSendBuffer;
	stMainClientSession* pSession;

	if (ServerBase_ZM::Valid(pSendBuffer, pSession, pSocket, packetID) != PACKET_VAILD::Success)
		return FALSE;

	pSession->RefreshAliveCheck();

	//-----------------------------------------------------------------------
	// RECV Packet

	UInt8 isFindCharacter = PX_RECV_GET_UI8(pBuffer);
	std::string findNickName = RecvPacketString::PX_RECV_GET_STRING_NICKNAME(pBuffer);

	//-----------------------------------------------------------------------
	// Send Packet

	if (isFindCharacter)
	{
		DBTryCatch::Try(DBTraceGetParam(&pSession, &findNickName)
		{
			sql::PreparedStatement* query = m_pThis->prepares[getFindCharacterList];

			DBTraceRecord(query->setUInt64, 1, pSession->account.ACGUID);
			findNickName = "%" + findNickName + "%";
			DBTraceRecordString(query->setString, 2, findNickName);			
			return query->execute();
		});

		if (DBTryCatch::IsExecuteResult())
		{
			sql::ResultSet* resultset = m_pThis->prepares[getFindCharacterList]->getResultSet();

			pSendBuffer->WriteUI8(PACKET_VAILD::Success);
			pSendBuffer->WriteUI8(isFindCharacter);
			pSendBuffer->WriteUI16((UINT16)resultset->rowsCount());

			while (resultset->next())
			{
				UInt64 CHGUID = resultset->getInt64(1);
				std::string name(resultset->getString(2).c_str());

				pSendBuffer->WriteUI64(CHGUID);
				pSendBuffer->WriteString((char*)name.c_str());

				pSendBuffer->WriteUI32(resultset->getInt(3));
				pSendBuffer->WriteI8(resultset->getInt(4));
				pSendBuffer->WriteI8(resultset->getInt(5));

			}
		}
		else
		{
			return ServerBase_ZM::ErrorSend(PACKET_VAILD::False_DB, "", pSendBuffer, pSession, packetID);
		}
	}
	else if (isFindCharacter == false)
	{
		// DB 
		DBTryCatch::Try(DBTraceGetParam(&pSession)
		{
			sql::PreparedStatement* query = m_pThis->prepares[getFindAllCharacterList];

			DBTraceRecord(query->setUInt64, 1, pSession->account.ACGUID);

			return query->execute();
		});

		if (DBTryCatch::IsExecuteResult())
		{
			sql::ResultSet* resultset = m_pThis->prepares[getFindAllCharacterList]->getResultSet();

			pSendBuffer->WriteUI8(PACKET_VAILD::Success);
			pSendBuffer->WriteUI8(isFindCharacter);
			pSendBuffer->WriteUI16((UINT16)resultset->rowsCount());

			while (resultset->next())
			{
				UInt64 CHGUID = resultset->getInt64(1);
				std::string nickname(resultset->getString(2).c_str());

				pSendBuffer->WriteUI64(CHGUID);
				pSendBuffer->WriteString((char*)nickname.c_str());
				pSendBuffer->WriteUI32(resultset->getInt(3));
				pSendBuffer->WriteI8(resultset->getInt(4));
				pSendBuffer->WriteI8(resultset->getInt(5));
			}
		}
		else
		{
			return ServerBase_ZM::ErrorSend(PACKET_VAILD::False_DB,"", pSendBuffer, pSession, packetID);
		}
	}

	g_pIOCPEngine->SendBuffer(pSendBuffer, packetID);

	return TRUE;
}
