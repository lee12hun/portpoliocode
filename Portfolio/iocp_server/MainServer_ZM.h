﻿#ifndef	_MAIN_SERVER_ZM_H_
#define	_MAIN_SERVER_ZM_H_

extern	PxIOCPEngine*		g_pIOCPEngine;
extern	TemplateManager		g_csTemplateManager;

class	MainServer_ZM : public ServerBase_ZM
{
public:
	static MainServer_ZM* m_pThis;
	MainClientSessionMap								clientSessionMap;
	stTryConnectServer									globalServerConnector;
	
	vector< pair<UINT64, std::string> > m_MapListInfo;
	
	stTryConnectServer									loginServerConnector;

	RepeatCallTimer<SERVER_RECONNECT_TRY_TIME>			connectRetryTime;

	RecommendZoneServer									recommendZoneServer;
	RecommendInstanceServer								recommendInstanceServer;
	RepeatCallTimer<SERVER_INFO_UPDATE_REQUEST_TIME>	svrInfoUpdateTime;

	AuthKeyTable										zoneServerAuthKeyTable;
	AuthKeyTable										instanceServerAuthKeyTable;

	InfoManager<stCharacterInfo>						characterInfoManager;
	InfoManager<stAccountInfo>							accountInfoManager;

	std::list<stEventSchedule*>							m_listEventSchedule[EVENT_GAME_TYPE_COUNT];
	std::list<stEventSchedule*>							m_listEventScheduleEnded[EVENT_GAME_TYPE_COUNT];

	BOOL												m_bRegionServerRole;

	CDBTable m_DBTable;

	CManagerTool m_ManagerTool;
	CShop	m_shop;
	CFriends m_friends;
	CAccountInventory m_accountInventory;
	CMail m_Mail;
	CCharacter_Info m_Character_Info;
	CCharacter m_character;
	CFriendChatting m_FriendChatting;
	CServerMessage m_ServerMessage;
	CAdMob m_AdMob;

	ServerProxies m_proxies;

	CMapData m_mapData;
	
	CPacketAliveCheck	clientAliveCheck;
	
	_MemoryPool<stPlayerCharacter>* m_pPoolPlayerCharacter;
	_MemoryPool<stAccountItem>* m_pPoolAccountItem;
	_MemoryPool<stCharacterItem>* m_pPoolCharacterItem;	
	_MemoryPool<stCharacterPreset>* m_pPoolCharacterPreset;
	_MemoryPool<stEventSchedule>* m_pPoolEventSchedule;

	// HGH EDN
	MainServer_ZM();
	~MainServer_ZM();

	BOOL												Initialize(UINT32 svrID, const char* servername) override;
	void												Release() override;
	void												Update() override;

	BOOL												InitDBCashes();

	BOOL												PrepareDBCommands(THMySqlConnector* pConn) override;		// DB Command들을 미리 준비하자.
	void												ReleaseDBCommands() override;

	void												InitServerInfos();
	BOOL												ConnectServers();
	void												FreeClientSessionData(stMainClientSession* pSession);

	void												ReleasePlayerCharacter(stPlayerCharacter* pPC);
	void												ReleaseAccountItem(stAccountItem* pACItem);
	void												ReleaseCharacterItem(stCharacterItem* pCHItem);
	void												ReleaseCharacterPreset(stCharacterPreset* pCHPreset);

	void												DBGet_Auth(stMainClientSession* pSession);				// Auth 후 DB 에서 데이터를 가져온다.
	void												DBGet_Connect(stMainClientSession* pSession);			// Connect 후 DB 에서 데이터를 가져온다.

	BOOL												InitGameDatas();
	void												ReleaseGameDatas();

	void												DBAddTestData(THMySqlConnector* pConn);

	void												DBProcessCreateCharacter(stAccount* pAC, stPlayerCharacter* pPC);
	void												DBProcessDeleteCharacter(stAccount* pAC, stPlayerCharacter* pPC);
	void												DBUpdateSlotIndexCharacter(stPlayerCharacter* pFirstSelectPC, stPlayerCharacter* pSecondSelectPC);
	void												DBReadAccountItemListBin(std::istream* blob, UINT16 blobSize, stAccount* pAC);
	void												DBReadCustomizeBin(std::istream* blob, UINT16 blobSize, stCharacter_Equip* pCH);
	void												DBReadCharacterItemListBin(std::istream* blob, UINT16 blobSize, stCharacter* pCH);
	void												DBGetByteArray_AccountItemListBin(stAccount* pAC, BYTE* pVal, int& valSize);
	void												DBGetByteArray_CharacterItemListBin(stCharacter* pCH, BYTE* pVal, int& valSize);

	void												UpdateSvrInfos();
	void												UpdateEventSchedule();

	// Socket Callback Functions
	void												InitCBFunction();									// CB 을 지정하자

	static	BOOL										OnConnectSocket(PVOID	pData);
	static	BOOL										OnCloseSocket(PVOID		pData);
	static	BOOL										OnDBServerLost(PVOID	pData);

	static	BOOL										ProcHandShake_v0(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);	
	static	BOOL										ProcConnectMainServerAuth_v0(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);
	static	BOOL										ProcAccountLogout_v0(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);

	static	BOOL										ProcServerEnd_v0(PX_SOCKET* pSocket, BYTE* pBuffer, int		buffLen);
	static	BOOL										ProcServerTypeAnnounce_v0(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);
	static	BOOL										ProcRequestTimeTag_v0(PX_SOCKET* pSocket, BYTE* pBuffer, int buffLen);
	static	BOOL										ProcInformTimeTag_v0(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);
	static	BOOL										ProcServerAliveCheck_v0(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);
	static	BOOL										ProcMainServerInfo_v0(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);
	static	BOOL										ProcMainServerConnectPrepare_v0(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);
	static	BOOL										ProcRequestNickNameReserve_v0(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);
	static	BOOL										ProcCancelNickNameReserve_v0(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);
	static	BOOL										ProcConfirmNickNameReserve_v0(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);
	static	BOOL										ProcDeleteNickName_v0(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);
	static	BOOL										ProcSetRegionServerRole_v0(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);
	static	BOOL										ProcAddEventSchedule_v0(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);
	static	BOOL										ProcModifyEventSchedule_v0(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);
	static	BOOL										ProcDeleteEventSchedule_v0(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);
};
#endif
