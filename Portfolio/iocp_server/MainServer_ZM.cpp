﻿#include "MainServerPCH.h"

#include <limits>

MainServer_ZM* MainServer_ZM::m_pThis = NULL;
TemplateManager	g_csTemplateManager;

MainServer_ZM::MainServer_ZM()
	: ServerBase_ZM()
{
	m_proxies.Add(&m_DBTable);

	m_proxies.Add(&m_shop);
	m_proxies.Add(&m_friends);
	m_proxies.Add(&m_Mail);
	m_proxies.Add(&m_Character_Info);
	m_proxies.Add(&m_accountInventory);
	m_proxies.Add(&m_character);
	m_proxies.Add(&m_FriendChatting);
	m_proxies.Add(&m_ServerMessage);
	m_proxies.Add(&m_ManagerTool);
	m_proxies.Add(&m_AdMob);
}

MainServer_ZM::~MainServer_ZM()
{
}

BOOL	MainServer_ZM::Initialize(UINT32 svrID, const char* servername)
{
	if (ServerBase_ZM::Initialize(svrID, servername) == FALSE)
	{
		return FALSE;
	}

	clientSessionMap.Initialize();

	g_pIOCPEngine->m_strDebugFileNameHeader = "main_zm";

	// CB setting
	g_pIOCPEngine->m_pSocketConnCB = OnConnectSocket;
	g_pIOCPEngine->m_pSocketDisconnCB = OnCloseSocket;
	g_pIOCPEngine->m_pDBServerLostCB = OnDBServerLost;

	InitCBFunction();
	m_proxies.InitCBFunction();
	

	g_pIOCPEngine->StartEngine(m_iMySvrPort, 2, 0, 0, 3, (m_aServerInfo[m_iMySvrIndex].port_client_UDP != 0) ? TRUE : FALSE, m_aServerInfo[m_iMySvrIndex].port_client_UDP);
	g_pIOCPEngine->SetPacketTypeCount(PK_COUNT, PKS_COUNT, PKU_COUNT);

	InitServerInfos();

	if (!ConnectServers())
	{
		printf("ConnectServers Failed\n");
		return FALSE;
	}

	zoneServerAuthKeyTable.Init();
	instanceServerAuthKeyTable.Init();
	
	accountInfoManager.Initialize(8192, "stAccountInfo");
	characterInfoManager.Initialize(8192, "stCharacterInfo");

	m_pPoolPlayerCharacter = new _MemoryPool<stPlayerCharacter>(8192, "stPlayerCharacter");
	m_pPoolAccountItem = new _MemoryPool<stAccountItem>(8192, "stAccountItem");
	m_pPoolCharacterItem = new _MemoryPool<stCharacterItem>(8192, "stCharacterItem");
	m_pPoolCharacterPreset = new _MemoryPool<stCharacterPreset>(8192, "stCharacterPreset");
	m_pPoolEventSchedule = new _MemoryPool<stEventSchedule>(8192, "stEventSchedule");

	if (!g_csTemplateManager.Initialize())
	{
		printf("g_csTemplateManager.Initialize Failed\n");
	}

	if (m_csDBConn[DB_COMMON].m_iEventScheduleAddCount > 0)
	{
		DBAddTestData(&m_csDBConn[DB_COMMON]);
	}

	//InitDBCashes();

	InitGameDatas();
	
	CDBAgentClient::Instance()->Initialize(static_cast<PVOID>(this));

	m_proxies.Initialize();
	m_mapData.Initialize(static_cast<PVOID>(this));

	return TRUE;
}

void	MainServer_ZM::Release()
{
	ReleaseGameDatas();

	m_proxies.Release();

	g_csTemplateManager.Release();

	clientSessionMap.Release();

	accountInfoManager.Release();
	characterInfoManager.Release();

	if (m_pPoolPlayerCharacter) { delete	m_pPoolPlayerCharacter; m_pPoolPlayerCharacter = NULL; }
	if (m_pPoolAccountItem) { delete	m_pPoolAccountItem; m_pPoolAccountItem = NULL; }
	if (m_pPoolCharacterItem) { delete	m_pPoolCharacterItem; m_pPoolCharacterItem = NULL; }
	if (m_pPoolCharacterPreset) { delete	m_pPoolCharacterPreset; m_pPoolCharacterPreset = NULL; }
	if (m_pPoolEventSchedule) { delete	m_pPoolEventSchedule; m_pPoolEventSchedule = NULL; }

	ServerBase_ZM::Release();

	CDBAgentClient::Instance()->Release();

	m_mapData.Release();

}

BOOL	MainServer_ZM::InitGameDatas()
{
	return TRUE;
}

void	MainServer_ZM::ReleaseGameDatas()
{

}

void	MainServer_ZM::Update()
{
	ServerBase_ZM::Update();

	clientAliveCheck.Update(m_iCurTickDiff);

	// Server Connect 시도
	ConnectServers();

	UpdateEventSchedule();

	g_pIOCPEngine->ProcRcvBuf_Client();
	g_pIOCPEngine->ProcRcvBuf_Server();
	g_pIOCPEngine->ProcSocketDisconnect();

	UpdateSvrInfos();

	CDBAgentClient::Instance()->Update();

	m_mapData.Update();


	ServerBase_ZM::PostUpdate();

	// HGH start
	
	CallBackCheckTime();  
	// HGH End
}

BOOL	MainServer_ZM::OnConnectSocket(PVOID	pData)
{
	PX_SOCKET* pSocket = (PX_SOCKET*)pData;

	return TRUE;
}

BOOL	MainServer_ZM::OnCloseSocket(PVOID	pData)
{
	PX_SOCKET* pSocket = (PX_SOCKET*)pData;

	if (pSocket->bInsideConnect)
	{
		int svrIndex = pSocket->SKUID;

		m_pThis->loginServerConnector.CloseSocket(m_pThis->m_aServerInfo[svrIndex].type);

		m_pThis->globalServerConnector.CloseSocket(m_pThis->m_aServerInfo[svrIndex].type);

		m_pThis->recommendZoneServer.CloseSocket(m_pThis->m_aServerInfo[svrIndex].type, pSocket);
		m_pThis->recommendInstanceServer.CloseSocket(m_pThis->m_aServerInfo[svrIndex].type, pSocket);
	}
	else
	{
		if (pSocket->pData)
		{
			stMainClientSession* pSession = (stMainClientSession*)pSocket->pData;

			if (pSession->statusReserveNickName != RNS_NOT_RESERVED)
			{
				PX_BUFFER* pToGlobalSvr = PxObjectManager::GetSendBufferObj(m_pThis->globalServerConnector.GetSocket());
				if (pToGlobalSvr)
				{
					pToGlobalSvr->WriteUI64(pSession->account.ACGUID);
					pToGlobalSvr->WriteString(pSession->reserveNickName_UTF8);
					g_pIOCPEngine->SendBuffer(pToGlobalSvr, PKS_CANCEL_NICKNAME_RESERVE);
				}
			}

			m_pThis->clientSessionMap.FreeClientSession(pSession);
			pSocket->pData = NULL;
		}
	}
	return TRUE;
}

void	MainServer_ZM::FreeClientSessionData(stMainClientSession* pSession)
{
	stAccount* pAccount = &pSession->account;
	std::list<stPlayerCharacter*>::iterator itor_pc = pAccount->listPCForLobby.begin();
	while (itor_pc != pAccount->listPCForLobby.end())
	{
		stPlayerCharacter* pPC = *itor_pc;
		ReleasePlayerCharacter(pPC);
		++itor_pc;
	}
	pAccount->listPCForLobby.clear();

	std::list<stAccountItem*>::iterator itor_ACItem = pAccount->listAccountItem.begin();
	while (itor_ACItem != pAccount->listAccountItem.end())
	{
		ReleaseAccountItem(*itor_ACItem);
		++itor_ACItem;
	}
	pAccount->listAccountItem.clear();

	if (pSession->connectState != SCS_DISCONNECT_BY_OTHER_CONN)
	{		
		clientSessionMap.Remove(pAccount->ACGUID);
	}

	pAccount->pAccountInfo = NULL;
}

void		MainServer_ZM::ReleaseAccountItem(stAccountItem* pACItem)
{
	m_pPoolAccountItem->Free(pACItem);
}

void		MainServer_ZM::ReleaseCharacterItem(stCharacterItem* pCHItem)
{
	m_pPoolCharacterItem->Free(pCHItem);
}

void		MainServer_ZM::ReleaseCharacterPreset(stCharacterPreset* pCHPreset)
{
	m_pPoolCharacterPreset->Free(pCHPreset);
}

void	MainServer_ZM::UpdateSvrInfos()
{
	if (svrInfoUpdateTime.IsCall(m_iCurrentTime))
	{
		recommendZoneServer.UpdateSvrInfos();
		recommendInstanceServer.UpdateSvrInfos();
	}
}
