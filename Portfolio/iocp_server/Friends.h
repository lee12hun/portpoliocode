#pragma once

struct stFriendList
{
	UInt64 m_CHGUID;
	UInt32 m_friendship_count;
	UInt32 m_bestfriend_state;
	std::string m_nickname;
	std::string m_memo;

	stFriendList(UInt64 guid,
	UInt32 friendship_count,
	UInt32 bestfriend_state,
	std::string nickname,
	std::string memo)
	{
		m_CHGUID = guid;
		m_friendship_count = friendship_count;
		m_bestfriend_state = bestfriend_state;
		m_nickname = nickname;
		m_memo = memo;

	}

};

class CFriends : public IServerProxyInstance<CFriends>
{
public:
	enum PrepareParams
	{
		// 찾기
		selectFindAllCharacterList,  // 캐릭터 리스트 
		selectFindCharacterList, //캐릭터 찾기 

		// 요청
		insertFriendRequest,  // 친구 요청
		selectFriendRequest_RecvList,  // 받은 요청
		selectFriendRequest_SendList,  // 보낸 요청
		selectFriendRequest_FindID, // 
		deleteFriendRequest,  // 친구 요청 취소

		// 등록
		insertFriend, // 친구 등록
		selectFriendList_CHGUID,			// 등록한 친구 목록
		selectFriendList_friend_CHGUID,	// 등록한 친구 목록
		selectFriendRegist_FindID,
		updateFriendMemo_memo,		// 등록한 친구 메모 작성
		updateFriendMemo_friend_memo,	// 등록한 친구 메모 작성
		updateFriendCount, // 전체 친구 수

		// 차단
		selectFriendList_Block,
		selectFriendList_Block_FindID,
		insertFriendBlock_CHGUID,  // 친구 차단 추가
		updateFriendBlock,
		deleteFriendBlock,  // 친구 차단 삭제
		updateFriendBlockCount, // 전체 차단 수
		
		selectCharacterFriendCount, // 친구수의 변화가 있는지 확인
		deletefriend,   // 친구 삭제
		selectFriendList_addcloumn, // 추가 적인 데이터 전송
		update_friend_request_confirm,		// 친구요청 수락
		update_friend_request_cancel,		// 친구요청 거절
		select_friend_request_info,			// 친구승인 후 상대방 데이터 값 가져오기 
		select_friend_list,					// CHGUID로 친구 GUID 가져오기
		update_friend_LogOut_Time,			// 로그아웃 할때 시간 저장.
		check_friend,						// 케릭터가 친구인지 확인 ( 0 아님, 1 친구) 
		COUNT
	};
	PrepareProxies prepares;	

public:
	CFriends();

	void Initialize();
	void Release();
	void InitCBFunction();
	void InitDBPrepare(THMySqlConnector* pConn);
	void ReleaseDBCommands();

	// MainServer Event
	void SendCharacterOfflineInfo(UINT64 Ui64_CHGUID);
	void SendCharacterDelete(UINT64 Ui64_CHGUID);	
	void SendCharacter_Profil_Update(UINT64 Ui64_Character_GUID, UINT8 Ui8_Profil_Type, UINT8 Ui8_Profil_ID);  // 친구 프로필 업데이트 

	// Packet
	static  BOOL ProcFriendFindList(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);
	static	BOOL ProcFriendRequest(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);
	static	BOOL ProcFriendRequestList(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);
	static	BOOL ProcFriendRequestCancel(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);
	static	BOOL ProcFriendReceivedList(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);
	static	BOOL ProcFriendReceivedConfirm(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);	
	static	BOOL ProcFriendRegistedList(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);	
	static	BOOL ProcFriendRegistedMemo(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);
	static	BOOL ProcFriendBlock(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);
	static	BOOL ProcFriendBlockList(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);
	static	BOOL ProcFriendRequestDelete(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);
	static	BOOL ProcFriendUnblock(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);
	static	BOOL ProcFriendDelete(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);
	static	BOOL ProcFriendRequestBring(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);
	static	BOOL ProcFriendRequestBringAgree(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);
	static	BOOL ProcFriendRequestChase(PX_SOCKET* pSocket, BYTE* pBuffer, int	buffLen);

	static BOOL SendFail(PX_BUFFER*& pSendBuffer, PACKET_VAILD errorType, string erromsg);
};