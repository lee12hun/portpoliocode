#pragma once


struct DB_AdminItem_Data
{
	UINT64 ITGUID;
	UINT32 ITGUID_Count;	
	THGUID_OBJ_TYPE TYPE;
	UINT64 TUID;	
};

class DB_AdminItem : public AdminTableList< DB_AdminItem_Data >
{
public:
	virtual void SetPrepared(THMySqlConnector* pConn) override
	{
		sql::Connection* pSqlConn = pConn->m_pConn;

		prepared = pSqlConn->prepareStatement("SELECT ITGUID, ITGUID_Count, THGUID_OBJ_TYPE, TUID FROM admin_item");
	}

	virtual void Load() override
	{
		SelectExecutor selectitem(DBExecutorDefaultMember(DB_AdminItem::prepared));

		sql::ResultSet* result = selectitem.Execute();

		if (result)
		{
			while (result->next())
			{
				UINT64 ITGUID = result->getUInt64(1);

				DB_AdminItem_Data* item = this->AddData(ITGUID);

				item->ITGUID = ITGUID;
				item->ITGUID_Count = result->getUInt(2);
				item->TYPE = (THGUID_OBJ_TYPE)result->getUInt(3);
				item->TUID = result->getUInt64(4);
			}
		}
	}
};



struct DB_AdminShop_Data
{
	UINT64 SHOPID;
	UInt32 isuse;

	time_t startDateTime;
	time_t endDateTime;

	Zemiverse_MoneyType priceMoneyType;
	UInt32 priceValue;
	//UInt32 reward_ItemType;
	UInt64 reward_Item_GUID;
	UInt32 reward_ItemCount;

	UInt32 limitValue;
	UInt32 limitResetType;
	UInt32 rewardUseTime;
};

class DB_AdminShop : public AdminTableList< DB_AdminShop_Data >
{
public:
	virtual void SetPrepared(THMySqlConnector* pConn) override
	{
		sql::Connection* pSqlConn = pConn->m_pConn;

		prepared = pSqlConn->prepareStatement("SELECT SHOPID, isuse, startDateTime, endDateTime, priceMoneyType, priceValue, reward_Item_GUID, reward_ItemCount, limitValue, limitResetType, rewardUseTime FROM admin_shop");
	}

	virtual void Load() override
	{
		SelectExecutor select_shop(DBExecutorDefaultMember(DB_AdminShop::prepared));

		SelectResultSet result_shop = select_shop.Execute();

		while (result_shop.Next())
		{
			UINT64 SHOPID = 0;
			result_shop.GetData(SHOPID);

			DB_AdminShop_Data* item = this->AddData(SHOPID);
			
			item->SHOPID = SHOPID;

			result_shop.GetData(item->isuse);

			result_shop.GetTime(item->startDateTime);
			result_shop.GetTime(item->endDateTime);

			result_shop.GetData(item->priceMoneyType);
			result_shop.GetData(item->priceValue);
			result_shop.GetData(item->reward_Item_GUID);
			result_shop.GetData(item->reward_ItemCount);

			result_shop.GetData(item->limitValue);
			result_shop.GetData(item->limitResetType);
			result_shop.GetData(item->rewardUseTime);
		}
	}
};

