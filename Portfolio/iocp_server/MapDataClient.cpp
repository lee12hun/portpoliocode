void FileServerMainThread(PVOID pData)
{
    ServerBase_ZM* pServer = static_cast<ServerBase_ZM*>(pData);

    _g_wsClient = new websocket_client();
    _g_wsClient->init_asio();
    _g_wsClient->set_access_channels(websocketpp::log::alevel::none);
    _g_wsClient->set_error_channels(websocketpp::log::alevel::none);

    _g_wsClient->set_open_handler(bind(&on_fs_open, _g_wsClient, &_g_hdl, ::_1));
    _g_wsClient->set_close_handler(bind(&on_fs_close, _g_wsClient, &_g_hdl, ::_1));
    _g_wsClient->set_message_handler(bind(&on_fs_message, _g_wsClient, ::_1, ::_2));

    for (int i = 1; g_bServerStop == false; i++)
    {
        try {
            g_pIOCPEngine->log.FPrint("Connecting FileServer(%s)...", _g_strUri.c_str());

            websocketpp::lib::error_code ec;
            _g_ConnectionPtr = _g_wsClient->get_connection(_g_strUri, ec);
            if (ec) {
                g_pIOCPEngine->log.FPrint("Connecting FileServer failed:%s", ec.message().c_str());
                break;
            }
            _g_wsClient->connect(_g_ConnectionPtr);
            _g_wsClient->run();

        }
        catch (websocketpp::exception const& e) {
            g_pIOCPEngine->log.FPrint(" FileServer catch:%s", e.what());
        }
        _g_bWebsocketConnected = false;
        _g_wsClient->stop();
        _g_wsClient->reset();
        Sleep(3000);
    }
}

void FileServerSendThread()
{
    do {
        int count = _g_sendQ.Size();
        while (--count >= 0 && g_bServerStop == false && _g_bWebsocketConnected && _g_sendQ.Size() > 0 && !_g_hdl.expired())
        {
            WebSocketMsg& msg = _g_sendQ.DeQueue();
            if (msg.processTime == -1)
            {
                if (g_bServerStart == FALSE)
                {
                    _g_sendQ.EnQueue(msg);
                    continue;
                }
            }
            else if (msg.processTime > GetTickCount())
            {
                _g_sendQ.EnQueue(msg);
                continue;
            }

            json j;
            j["type"] = msg.msgType;
            j["value"] = msg.msgValue;

            g_pIOCPEngine->log.FPrint("FileServer send:" + msg.msgType);

            websocketpp::lib::error_code ec;
            _g_wsClient->send(_g_hdl, websocketpp::base64_encode(j.dump()), websocketpp::frame::opcode::text, ec);
            if (ec) {
                g_pIOCPEngine->log.FPrint("FileServer send failed(%d):%s", _g_sendQ.Size(), ec.message().c_str());
                Sleep(1000);
                break;
            }
        }
        Sleep(1);
    } while (g_bServerStop == false);
}

void SendMapData(WebSocketMsg& msg)
{
    _g_sendQ.EnQueue(msg);
}

void CMapDataClient::Initialize(PVOID pData)
{
    m_pMainThread = new std::thread(FileServerMainThread, pData);
    m_pSendThread = new std::thread(FileServerSendThread);
}

void CMapDataClient::Release()
{
    m_pMainThread->join();
    m_pSendThread->join();
}

void CMapDataClient::SetPacketCB(std::string type, PxSvrCallBackFunc pFunc)
{
    m_mapPacketFunc[type] = pFunc;
}

void CMapDataClient::Update()
{
    ProcMessage();
    if (g_bServerStop)
        _g_wsClient->stop();
}

void CMapDataClient::recvEnQueue(WebSocketMsg& msg)
{
    _g_recvQ.EnQueue(msg);
}

void CMapDataClient::ProcMessage()
{
    int count = _g_recvQ.Size();
    for (int i = 0; i < count; i++)
    {
        WebSocketMsg msg = _g_recvQ.DeQueue();

        if (msg.processTime == -1)
        {
            if (g_bServerStart == FALSE)
            {
                _g_recvQ.EnQueue(msg);
                continue;
            }
        }
        else if (msg.processTime > GetTickCount())
        {
            _g_recvQ.EnQueue(msg);
            continue;
        }

        auto it = m_mapPacketFunc.find(msg.msgType);
        if (it != m_mapPacketFunc.end())
        {
            it->second((PVOID)(&msg.msgValue));
        }
    }
}

#endif