/* eslint-disable no-fallthrough */
/* eslint-disable no-unused-vars */
'use strict';

const glo = require('./global');
const wsServer = require('ws').Server;

const wss = new wsServer({ port: glo.server_config.server.ws_port });

const EventEmitter = require('events');
const ws = new EventEmitter();
const { CClient, GetClient } = require('./client');
const { Log, LogErr } = require('./logger');
const { ServerStop } = require('./service');
const util = require('./util');

var common = require('./routes_dev/common/common');
var DBSchema = require('./db_mongoose.js');

require('json-bigint-patch');

let g_SocketSN = 0;

Log('Server opened on port ' + glo.server_config.server.ws_port);

function SocketInit(socket) {
    if (socket._socket == undefined || socket._socket.remoteAddress == undefined || socket._socket.remotePort == undefined) {
        return false;
    }
    socket.nick = ''; //DEF socket
    socket.PingTime = glo.currTime;
    socket.LastPingTime = glo.currTime;
    socket.IP = socket._socket.remoteAddress.replace("::ffff:", "");
    socket.port = socket._socket.remotePort;
    socket.SID = socket.IP + ':' + socket.port + '-' + ++g_SocketSN;
    return true;
}

/**
 @param bDisconnect {Boolean}
*/
function Disconnect(socket, bDisconnect) {
    
    let client = GetClient( socket.nick);
    if (!client) {
        if (bDisconnect)
            socket.close();
        return;
    }
    if (client.GetSID() == socket.SID)
        client.OnDisconnect(bDisconnect);
}

wss.on('connection', socket => {
    if (!SocketInit(socket))
        return;
    Log('connected:' + socket.SID);

    socket.on('close', () => {
        Log('disconnected:' + socket.SID);
        Disconnect(socket, false);
    });

    socket.on('message', _msg => {
        let bClient = false;
        let client;

        if (socket.nick && socket.nick != '') {
            client = GetClient( socket.nick);
            if (client) {
                bClient = true;
                client.lastPingTime = glo.currTime;
            }
        }
        let msg = util.Base64Decode(_msg.toString()).toString();;
        try {
            let js = JSON.parse(msg);
            if(js.value == undefined)
                js.value = {};
            if (js.type != undefined && js.value != undefined) {
                if (bClient) {
                    if(client.IsRegisterd())
                    {
                        client.ClientLog('recv:' + js.type);
                        ws.emit(js.type, client, js.value);
                    }
                }
                else {
                    if(js.type == 'ReqRegister')
                    {
                        Log('recv:' + js.type);
                        ws.emit(js.type, socket, js.value);
                    }
                }
            }
            else {
                if (glo.GetDevID()) {
                    LogErr(msg);
                    LogErr('on message: unknown msg');
                }
            }
        }
        catch (e) {
            if (glo.GetDevID()) {
                LogErr(msg);
                LogErr(e.message);
                LogErr(e.stack);
            }
        }
    });
});

ws.on('ReqRegister', (socket, msg) => {
    if(msg.server_ip == undefined || msg.server_port == undefined || msg.server_type == undefined)
        return;

    let id = socket.SID;
    let nick = msg.server_ip + '_' + msg.server_port + '_' + msg.server_type;

    let client = new CClient(socket, id, nick);
    client.Register();
});

//------------------------------------------------------------------------------------------------------------------
// web socket packet 
//------------------------------------------------------------------------------------------------------------------
ws.on('PK_MAPDATA_LOAD', (/**@type  CClient*/client, msg) => {
    //
    let packetID = 'PK_MAPDATA_LOAD';

    let str = packetID + ', ' + client.nick + ', ' + JSON.stringify(msg);
    client.ClientLog(str);
    Log(str);

    // is vaild
    if(common.isNull(msg.requestACGUID) || common.isNull(msg.MAPGUID) ) 
    {
        send.message = 'error-undefined ';
        client.Send(packetID, send);
        return;
    }

    // send packet    
    let send = {
        isvaild : false,
        requestACGUID : msg.requestACGUID
    };

    // find key 
    let MapKey = {
        MAPGUID : msg.MAPGUID
    };

    const arrData = DBSchema.MapData.find(MapKey);

    DBSchema.MapData.find(MapKey).exec()
    .then(find_rows=>{            
        if(find_rows.length > 0) 
        {
            send.isvaild = true;
            send.results = find_rows;
            client.Send(packetID, send);
        }            
        else
        {
            send.isvaild = false;
            send.message = ' find data nothing ';
            client.Send(packetID, send);
        }
    })	
    .catch(error=>{
        send.message = 'error-find : ' + error;
        client.Send(packetID, send);
    });
});

ws.on('PK_MAPDATA_PURCHASE', function(/**@type  CClient*/client, msg) {

    let packetID = 'PK_MAPDATA_PURCHASE';

    let str = packetID + ', ' + client.nick + ', ' + JSON.stringify(msg);
    client.ClientLog(str);
    Log(str);

    // is vaild
    if(common.isNull(msg.BuyerACGUID) || common.isNull(msg.MAPGUID) ) 
    {
        send.message = 'error-undefined ';
        client.Send(packetID, send);
        return;
    }

    let send = {
        isvaild : false,
        requestACGUID : msg.BuyerACGUID
    };

	let MapKey = {
        MAPGUID : msg.MAPGUID,      // req.body.MAPGUID,
        GroundTID : msg.GroundTID   // req.body.GroundTID
    };

	DBSchema.MapData.findOne(MapKey).exec()
	.then(find_row => {
		if(common.isNull(find_row))
        {
			return DBSchema.MapData.create(msg);
		}
		else{
            const res = DBSchema.MapData.updateOne(MapKey,msg);
            if(res.acknowledged)
                return msg;
            else
                return null;
		}
	})
	.then(result_row=>{

        if(result_row)
        {   
            send.isvaild = true;
            
            send.results = result_row;
            client.Send(packetID, send);
        }
		else
        {
            send.message = 'data nothing : ';
            client.Send(packetID, send);
        }
			
	})
	.catch(error=>
    {
        send.message = 'error : ' + error;
        client.Send(packetID, send);
	});

});