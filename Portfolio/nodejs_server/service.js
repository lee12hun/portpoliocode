/* eslint-disable no-unused-vars */
'use strict';
const glo = require('./global');
const { Log, LogErr } = require('./logger');
const { GetClient } = require('./client');

function ServerStop() {
    glo.bServerOK = false;

    for (let nick in glo.clients) {
        let client = GetClient(nick);
        client.Send('ServerStop', {});
        client.OnDisconnect();
    }

    setTimeout(() => {
        LogErr('Server stopped!');
        glo.logStream.end();
        glo.logErrStream.end();
        process.exit(0);
    },  10000);
}

module.exports = { ServerStop };