/* eslint-disable no-unused-vars */
'use strict';
const fs = require('fs');
const glo = require('./global');
const util = require('./util');
const { Log, LogErr } = require('./logger');
//const db = require('./db');

function SetData(t, data, i) {
    let channo = t * 100 + i + 1;
    glo.chans[channo] = {} //DEF chan
    glo.chans[channo].chanID = channo;
    glo.chans[channo].chanName = data[i][0];
    glo.chanTypes[t].push(glo.chans[channo]);
}

function LoadData() {
    return new Promise( (resolve) => {
        let datafile = 'data.csv';
        if(util.IsFileExists(datafile)) {
            const string_csv = fs.readFileSync('data.csv').toString();
            const data = util.csvParse(string_csv);
            glo.chanTypes[6] = [];
            for (let i = 0; i <= 100 && i < data.length; i++) {
                if (data[i][0].length < 2)
                    break;
                SetData(6, data, i);
            }
        }
        resolve();
    });
}

let g_Tasks = [
    function (callback) {
        (async () => {
            await LoadData();
            glo.currDate = new Date();
            glo.currTime = glo.currDate.getTime();
            callback(null);
        })();
    },

    function (callback) {
        db.mysqlConn();
        //glo.gameDB.query('select 1', function (err, rows) {
        let Nums = [1,3,5];
        glo.gameDB.query('SELECT Quiz_Index, Quiz_Solution FROM event_oxquiz where Quiz_Index in (?)', [Nums], function (err, rows) {            
            if (err) {
                LogErr(err);
                glo.bServerOK = false;
            }
            else {
                glo.bServerOK = true;
            }
            callback(null);
        });
    },
];

module.exports = { g_Tasks }