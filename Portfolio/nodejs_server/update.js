/* eslint-disable no-unused-vars */
'use strict';

const glo = require('./global');
const { GetClient } = require('./client');

let g_lastUpdateTime100ms = glo.currTime;
let g_lastUpdateTime1sec = glo.currTime;
let g_lastUpdateTime5sec = glo.currTime;
let g_lastUpdateTime10sec = glo.currTime;
let g_lastUpdateTime30sec = glo.currTime;
let g_lastUpdateTime1min = glo.currTime;

const Update = () => {
    glo.currDate = new Date();
    glo.currTime = glo.currDate.getTime();

    let d = glo.currDate;
    glo.currDateTimeStr = glo.GetDateTimeStr(d);
    if (glo.currTime >= g_lastUpdateTime100ms + 100) {
        g_lastUpdateTime100ms = glo.currTime;

        for (let nick in glo.clients) {
           let client = GetClient(nick);
           client.Update();
        }
    }
    else return;

    if (glo.currTime >= g_lastUpdateTime1sec + 1000) {
        g_lastUpdateTime1sec = glo.currTime;
    }
    else return;

    if (glo.currTime >= g_lastUpdateTime5sec + 5000) {
        g_lastUpdateTime5sec = glo.currTime;
    }
    else return;

    if (glo.currTime >= g_lastUpdateTime10sec + 10000) {
        g_lastUpdateTime10sec = glo.currTime;
    }
    else return;

    if (glo.currTime >= g_lastUpdateTime30sec + 30000) {
        g_lastUpdateTime30sec = glo.currTime;
    }
    else return;

    if (glo.currTime >= g_lastUpdateTime1min + 60000) { 
        g_lastUpdateTime1min = glo.currTime;

        let datestr = glo.GetDateStr(glo.currDate);
        if (datestr != glo.currDateStr) {
            glo.currDateStr = datestr;
        }
    }
    else return;
}

module.exports = { Update };