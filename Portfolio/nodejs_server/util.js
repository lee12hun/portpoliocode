const fs = require('fs');
const glo = require('./global');

class CQueue {
    constructor() {
      this.elements = {};
      this.head = 0;
      this.tail = 0;
    }
    enqueue(element) {
      this.elements[this.tail] = element;
      this.tail++;
    }
    dequeue() {
      const item = this.elements[this.head];
      delete this.elements[this.head];
      this.head++;
      return item;
    }
    peek() {
      return this.elements[this.head];
    }
    length() {
      return this.tail - this.head;
    }
    isEmpty() {
      return this.length === 0;
    }
}

class CStack {
    constructor() {
      this.arr = [];
      this.index = 0;
    }
    push(item) {
      this.arr[this.index++] = item;
    }
    pop() {
      if (this.index <= 0) return null;
      const result = this.arr[--this.index];
      return result;
    }
}

/**
   * @param {String} path
 */
const IsFileExists = (path) => {
    try {
        if (fs.existsSync(path)) {
            return true;
        }
    } catch(err) {
        return false;
    }
}

const _createDir = (dir) => {
    try {
        fs.accessSync(dir, fs.F_OK);
    } catch (e) {
        fs.mkdirSync(dir);
    }
}

/**
   * @param {String} _dir
 */
const CreateDir = (_dir) => {
    let dir = _dir;
    let dir1 = '';
    while (true) {
        var idx = dir.indexOf(glo.conf.slash);
        if (idx == -1) {
            _createDir(_dir);
            break;
        }
        if (dir1 == '')
            dir1 = dir.slice(0, idx);
        else {
            dir1 = dir1 + glo.conf.slash + dir.slice(0, idx);
        }
        dir = dir.slice(idx + 1);

        _createDir(dir1);
    }
}

/**
   * @param {String} dir
   * @param {String} file
 */
const CreateFile = (dir, file) => {
    CreateDir(dir);

    let Stream = fs.createWriteStream(dir + glo.conf.slash + file, {
        'flags': 'a+',
        'encoding': "utf8",
    });

    return Stream;
}

/**
   * @param {Number} ms
 */
async function Sleep(ms) {
    await new Promise(resolve => setTimeout(resolve, ms));
}

/**
   * @param {String} csv_string
 */
const csvParse = (csv_string) => {
    const rows = csv_string.split("\r\n");
    const arr = [];
    for (let i = 1; i < rows.length; i++) {
        arr.push(rows[i].split(","));
    }
    return arr;
}

/**
   * @param {String} str
 */
const Base64Decode = (str) => {
    //return Buffer.from(str, "base64").toString('utf8');
    return Buffer.from(str, "base64");
}

/**
   * @return {String} 
 */
const Base64Encode = (buf) => {
    return Buffer.from(buf).toString('base64');
}

module.exports = { CQueue, CStack, CreateDir, CreateFile, IsFileExists, Sleep, csvParse, Base64Encode, Base64Decode }